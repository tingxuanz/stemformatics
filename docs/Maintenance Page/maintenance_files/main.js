
function filterGeneSymbol(geneSymbol,delimiter){
    
    var tempSymbol = geneSymbol.split(' ');
    var showSymbol = tempSymbol[0];
    
    for (var i =1;i<tempSymbol.length;++i){
        
        var thisSymbol = tempSymbol[i];
        
        if (thisSymbol.search(/LOC[0-9]{1,10}?/)) {
            showSymbol =showSymbol +delimiter+ thisSymbol;
        }
    }    
    
    return showSymbol;
    
}

function filterGeneDescription(geneSymbol,geneDescription){
    
    delimiter = '&lt;br /&gt;';
    
    var tempSymbol = geneSymbol.split(' ');
    var tempDescription = geneDescription.split(delimiter);
    var showDescription = tempDescription[0];
    
    for (var i =1;i<tempSymbol.length;++i){
        
        var thisSymbol = tempSymbol[i];
        if (thisSymbol.search(/LOC[0-9]{1,10}?/)) {
            showDescription =showDescription +'<br />'+ tempDescription[i];
        }
    }    
    
    return showDescription;
    
}


function contextHelpClick(){
    
    $('a.helpPopup').click(function(event){
            
        if(event.preventDefault) { event.preventDefault();} else {  event.returnValue = false; }
        
        window.open($(this).attr('href'), "myWindow", "status = 1, height = 860, width = 1024, resizable = 0, scrollbars=1" );
    
    });
}




// could put this into it's own javascript for readability
$(function () {

    $('#searchList div.search').mouseover(function(){
        $(this).addClass('hoverState').children().addClass('hoverState');
    }).mouseout(function(){
        $(this).removeClass('hoverState').children().removeClass('hoverState');
    });  

    $('#searchList div.search').click(function(){
        window.location = $(this).children('a').attr('href');
    });

    $('#searchListSide div.search').mouseover(function(){
        $(this).addClass('hoverState').children().addClass('hoverState');
    }).mouseout(function(){
        $(this).removeClass('hoverState').children().removeClass('hoverState');
    });  

    $('#searchListSide div.search').click(function(){
        window.location = $(this).children('a').attr('href');
    });

    $('div.showText').mouseover(function(){
        $(this).children().addClass('hoverState').children().addClass('hoverState');
        
    }).mouseout(function(){
        $(this).children().removeClass('hoverState').children().removeClass('hoverState');
    });

    /* setup all the links to confirm that you are going to another site */
    $('a[target=_blank]').click(function(){
            var answer = confirm("The link you clicked is opening up another window/tab. Is that OK?");
            
            if (!answer) {
                return false;
            }
        });
        
    /* remove the recaptcha what's this from the confirmation */
    if ($('#recaptcha_whatsthis_btn')) {
        $('#recaptcha_whatsthis_btn').unbind('click');
    }
    
});
    
