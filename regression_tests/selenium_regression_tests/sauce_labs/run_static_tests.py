
# ################################################################
# Need to add in the function above that has all the details here 
# It must be named run_tests eg.
# def run_tests(self):
#     driver = self.driver 
# ################################################################
def run_tests(self):
    driver = self.driver
    driver.get(self.base_url + "/expressions/result?graphType=scatter&datasetID=5003&gene=ENSG00000243137&db_id=56")
    driver.get(self.base_url + "/expressions/feature_result?graphType=default&db_id=46&feature_type=miRNA&feature_id=mmu-mir-205&datasetID=6128")
    driver.get(self.base_url + "/expressions/result?graphType=default&gene=ENSMUSG00000027547&db_id=46&datasetID=6130#")
    driver.get(self.base_url + "/expressions/result?graphType=default&datasetID=6151&gene=ENSMUSG00000035206&db_id=46#")
    driver.get(self.base_url + "/expressions/result?graphType=default&gene=ENSMUSG00000074637&db_id=46&datasetID=6131")
    driver.get(self.base_url + "/contents/speed_test")
    driver.get(self.base_url + "/datasets/search?filter=private")
    driver.get(self.base_url + "/genes/search?gene=mincl")
    driver.get(self.base_url + "/genes/search?gene=mincl&ensembl_id=ENSG00000166523")
    driver.get(self.base_url + "/workbench/gene_set_bulk_import_manager")
    driver.get(self.base_url + "/expressions/feature_result?graphType=default&feature_type=miRNA&feature_id=mmu-mir-148b&db_id=46")
    driver.get(self.base_url + "/probes/multi_map_summary?probe_id=ILMN_1665632&chip_type=7&db_id=56")
    driver.get(self.base_url + "/expressions/result?graphType=box&datasetID=5005&gene=POU&db_id=56&sortBy=Sample%20Type")
    driver.get(self.base_url + "/genes/search?gene=jjjjj")
    driver.get(self.base_url + "/datasets/search?filter=jjjj")
    driver.get(self.base_url + "/hamlet/index?dataset=5032")
    driver.get(self.base_url + "/expressions/result?graphType=default&gene=ENSG00000204531&db_id=56&datasetID=5005")
    driver.get(self.base_url + "/expressions/result?graphType=default&datasetID=6124&gene=ENSMUSG00000030142&db_id=46")
    driver.get(self.base_url + "/project_grandiose")
    driver.get(self.base_url + "/genes/search?gene=POU5F1")
    driver.get(self.base_url + "/expressions/result?graphType=box&datasetID=6138&gene=ENSMUSG00000024406&db_id=46&sortBy=Sample%20Type")
    driver.get(self.base_url + "/expressions/probe_result?graphType=default&datasetID=6081&probe=ACVR2B&db_id=56")
    driver.get(self.base_url + "/expressions/result?graphType=default&datasetID=5005&gene=ENSG00000229094&db_id=56")
    driver.get(self.base_url + "/datasets/search?filter=2000&ds_id=2000")
    driver.get(self.base_url + "/expressions/probe_result?graphType=default&datasetID=5005&probe=ILMN_1912512&db_id=56")
    driver.get(self.base_url + "/expressions/feature_result?graphType=default&feature_type=miRNA&feature_id=mmu-mir-27b&db_id=46&datasetID=6128")
    driver.get(self.base_url + "/workbench/histogram_wizard?graphType=default&db_id=56&gene_set_id=779&datasetID=5005")
    driver.get(self.base_url + "/expressions/multi_dataset_result?graphType=scatter&gene=ENSG00000229094&db_id=56")
    driver.get(self.base_url + "/genes/summary?gene=ENSG00000229094&db_id=56")
    driver.get(self.base_url + "/workbench/gene_neighbour_wizard?gene=ENSG00000115415&db_id=56&datasetID=5005")
    driver.get(self.base_url + "/expressions/result?graphType=default&datasetID=5005&gene=ENSG00000229094&db_id=56&select_probes=ILMN_1679060")
    driver.get(self.base_url + "/workbench/histogram_wizard?graphType=default&db_id=56&gene_set_id=779&datasetID=5005&select_probes=ILMN_1711899|ILMN_1740938|ILMN_1694877")
    driver.get(self.base_url + "/workbench/rohart_msc_graph?ds_id=6037")
    driver.get(self.base_url + "/mappings/mapping_1.txt")







import os
import sys
import httplib
import base64
import json
import new
import unittest
import sauceclient
from selenium import webdriver
from sauceclient import SauceClient

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
import unittest, time, re

# Testing data stored in test_data.py
from defaults.test_data import test_data
test_data_object = test_data()
USERNAME = test_data_object.USERNAME
ACCESS_KEY = test_data_object.ACCESS_KEY
sauce = SauceClient(USERNAME, ACCESS_KEY)
browsers = test_data_object.browsers
base_url = test_data_object.base_url

def on_platforms(platforms):
    def decorator(base_class):
        module = sys.modules[base_class.__module__].__dict__
        for i, platform in enumerate(platforms):
            d = dict(base_class.__dict__)
            d['desired_capabilities'] = platform
            name = "%s_%s" % (base_class.__name__, i + 1)
            module[name] = new.classobj(name, (base_class,), d)
    return decorator

@on_platforms(browsers)
class NewClassNameHere(unittest.TestCase):
    # ################################################################
    # Need to add in the new class that has all the details here
    # ################################################################
    def test_to_run(self):
        run_tests(self)

    def setUp(self):
        self.desired_capabilities['name'] = self.id()

        self.base_url = base_url
        sauce_url = "http://%s:%s@ondemand.saucelabs.com:80/wd/hub"
        self.driver = webdriver.Remote(
            desired_capabilities=self.desired_capabilities,
            command_executor=sauce_url % (USERNAME, ACCESS_KEY)
        )
        self.driver.implicitly_wait(30)

   
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        print("Link to your job: https://saucelabs.com/jobs/%s" % self.driver.session_id)
        try:
            if sys.exc_info() == (None, None, None):
                sauce.jobs.update_job(self.driver.session_id, passed=True)
            else:
                sauce.jobs.update_job(self.driver.session_id, passed=False)
        finally:
            self.driver.quit()




if __name__ == "__main__":
    unittest.main()
