from guide.model.stemformatics import *
from pylons import config
from guide.model.error import GeneralS4MError

uid = 3 #rowlandm
param_view_by=0 #yugene default
param_show_lower='Dataset' #yugene default

def test_yugene_human():
    ensembl_id = 'ENSG00000111640'
    db_id = 56
    result = basic(ensembl_id,db_id)
    assert result == True

def test_yugene_mouse():
    ensembl_id = 'ENSMUSG00000030142'
    db_id = 46
    result = basic(ensembl_id,db_id)
    assert result == True


def basic(ensembl_id,db_id):
     
    all_sample_metadata = Stemformatics_Expression.setup_all_sample_metadata()

    assert len(all_sample_metadata) != 0
    yugene_granularity_for_gene_search = 'full'

    role = 'admin'
    result = Stemformatics_Expression.return_yugene_graph_data(db_id,uid,ensembl_id,all_sample_metadata,role)

    return result

def exception_test():
    dict_test = {'21':21}
    try:
        test = dict_test['Testing email of exception']
    except Exception as e:
        try:
            raise GeneralS4MError(e)
        except:
            pass
