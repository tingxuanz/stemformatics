from guide.model.stemformatics import *
from pylons import config
from guide.model.error import GeneralS4MError
from guide.model.graphs import *



uid = 3 #rowlandm
param_view_by=0 #yugene default
param_show_lower='Dataset' #yugene default


class tempData(object):
    pass


species_dict = Stemformatics_Gene.get_species(db)

temp = tempData() 

def test_mouse_geg():
    temp.ds_id = 4000
    temp.db_id = 46
    temp.ensembl_id = 'ENSMUSG00000030142'
    temp.ref_type = 'ensemblID'
    temp.ref_id = temp.ensembl_id
    temp.sortBy = 'Sample Type'
    temp.list_of_samples_to_remove = [] 
    temp.line_graph_available = False

    temp.expected_chip_type = 1
    temp.expected_data_levels = 2
    temp.expected_handle ='Asselin-Labat_2010_20383121' 
    temp.expected_x_axis_label = 'MaSC enriched (CD29 hi CD24+)'
    temp.expected_title_id = ' for Gene Clec4e'
    temp.expected_xaxis_label_type_bar_and_box = 'Probe'
    temp.expected_disease_x_axis_label = 'normal' 


    setup_graphs(temp)

def test_non_mapping_human_geg():
    temp.ds_id = 2000
    temp.db_id = 56
    temp.ensembl_id = 'ENSG00000185607' # there are no probes for this ensembl gene
    temp.ref_type = 'ensemblID'
    temp.ref_id = temp.ensembl_id
    temp.sortBy = 'Sample Type'
    temp.list_of_samples_to_remove = [] 
    temp.line_graph_available = False

    temp.expected_chip_type = 21
    temp.expected_data_levels = None 
    temp.expected_handle ='Matigian_2010_20699480' 
    temp.expected_x_axis_label = {}
    temp.expected_title_id = ' for Gene ACTBP7'
    temp.expected_xaxis_label_type_bar_and_box = {}
    temp.expected_disease_x_axis_label = {}



    this_graph_data = Graph_Data(db,temp.ds_id,temp.ref_type,temp.ref_id,temp.db_id,temp.list_of_samples_to_remove,species_dict)
    assert this_graph_data.handle == temp.expected_handle 
    assert this_graph_data.ref_type == temp.ref_type
    assert this_graph_data.ref_id == temp.ref_id
    assert this_graph_data.db_id == temp.db_id
    assert this_graph_data.chip_type == temp.expected_chip_type



    this_graph = Scatterplot_Graph(this_graph_data,temp.sortBy) 
    assert isinstance(this_graph.plot_data,dict)
    assert temp.expected_x_axis_label == this_graph.xaxis_labels
    assert not hasattr(this_graph,'data_levels')

    this_view = Preview(this_graph,temp.line_graph_available)
    assert this_view.title_id == temp.expected_title_id
    assert this_view.ensembl_id == temp.ensembl_id
    assert this_view.line_graph_available == False
    assert temp.expected_x_axis_label == this_view.graph.xaxis_labels
    assert not hasattr(this_view.graph,'data_levels')


    this_graph = Box_Graph(this_graph_data,temp.sortBy) 
    assert not hasattr(this_graph,'plot_data')
    assert not hasattr(this_graph,'xaxis_labels')
    assert not hasattr(this_graph,'data_levels')

    this_view = Preview(this_graph,temp.line_graph_available)
    assert this_view.title_id == temp.expected_title_id
    assert this_view.ensembl_id == temp.ensembl_id
    assert this_view.line_graph_available == False
    assert not hasattr(this_view.graph,'xaxis_labels')
    assert not hasattr(this_view.graph,'data_levels')





def test_human_geg():
    temp.ds_id = 2000
    temp.db_id = 56
    temp.ensembl_id = 'ENSG00000166523'
    temp.ref_type = 'ensemblID'
    temp.ref_id = temp.ensembl_id
    temp.sortBy = 'Sample Type'
    temp.list_of_samples_to_remove = [] 
    temp.line_graph_available = False

    temp.expected_chip_type = 21
    temp.expected_data_levels = 2
    temp.expected_handle ='Matigian_2010_20699480' 
    temp.expected_x_axis_label = 'Dermal Fibroblast'
    temp.expected_title_id = ' for Gene CLEC4E'
    temp.expected_xaxis_label_type_bar_and_box = 'Probe'
    temp.expected_disease_x_axis_label = 'control' 

    setup_graphs(temp)

def setup_graphs(temp):
    this_graph_data = Graph_Data(db,temp.ds_id,temp.ref_type,temp.ref_id,temp.db_id,temp.list_of_samples_to_remove,species_dict)
    assert this_graph_data.handle == temp.expected_handle 
    assert this_graph_data.ref_type == temp.ref_type
    assert this_graph_data.ref_id == temp.ref_id
    assert this_graph_data.db_id == temp.db_id
    assert this_graph_data.chip_type == temp.expected_chip_type


    this_graph = Scatterplot_Graph(this_graph_data,temp.sortBy) 
    assert isinstance(this_graph.plot_data,dict)
    assert temp.expected_x_axis_label in this_graph.xaxis_labels
    assert this_graph.data_levels == temp.expected_data_levels

    this_view = Preview(this_graph,temp.line_graph_available)
    assert this_view.title_id == temp.expected_title_id
    assert this_view.ensembl_id == temp.ensembl_id
    assert this_view.line_graph_available == False
    assert temp.expected_x_axis_label in this_view.graph.xaxis_labels
    assert this_view.graph.data_levels == temp.expected_data_levels


    this_graph = Box_Graph(this_graph_data,temp.sortBy) 
    assert isinstance(this_graph.plot_data,dict)
    assert this_graph.xaxis_labels[0]['type'] == temp.expected_xaxis_label_type_bar_and_box
    assert this_graph.data_levels == temp.expected_data_levels

    this_view = Preview(this_graph,temp.line_graph_available)
    assert this_view.title_id == temp.expected_title_id
    assert this_view.ensembl_id == temp.ensembl_id
    assert this_view.line_graph_available == False
    assert this_view.graph.xaxis_labels[0]['type'] == temp.expected_xaxis_label_type_bar_and_box
    assert this_view.graph.data_levels == temp.expected_data_levels


    this_graph = Bar_Graph(this_graph_data,temp.sortBy) 
    assert isinstance(this_graph.plot_data,dict)
    assert this_graph.xaxis_labels[0]['type'] == temp.expected_xaxis_label_type_bar_and_box
    assert this_graph.data_levels == temp.expected_data_levels

    this_view = Preview(this_graph,temp.line_graph_available)
    assert this_view.title_id == temp.expected_title_id
    assert this_view.ensembl_id == temp.ensembl_id
    assert this_view.line_graph_available == False
    assert this_view.graph.xaxis_labels[0]['type'] == temp.expected_xaxis_label_type_bar_and_box
    assert this_view.graph.data_levels == temp.expected_data_levels



    # disease state
    temp.sortBy = 'Disease State'
    this_graph = Scatterplot_Graph(this_graph_data,temp.sortBy) 
    assert isinstance(this_graph.plot_data,dict)
    assert temp.expected_disease_x_axis_label in this_graph.xaxis_labels
    assert this_graph.data_levels == temp.expected_data_levels

    this_view = Preview(this_graph,temp.line_graph_available)
    assert this_view.title_id == temp.expected_title_id
    assert this_view.ensembl_id == temp.ensembl_id
    assert this_view.line_graph_available == False
    assert temp.expected_disease_x_axis_label in this_view.graph.xaxis_labels
    assert this_view.graph.data_levels == temp.expected_data_levels

    # line graph - leave for now.



