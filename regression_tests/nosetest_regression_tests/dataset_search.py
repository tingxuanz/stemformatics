from guide.model.stemformatics import *
from guide.model.graphs import *
import psycopg2
import psycopg2.extras


class tempData(object):
    pass

temp = tempData() 



def get_ds_ids_for_dataset_search(temp):
    uid = temp.uid
    search = temp.search
    filter_dict = temp.filter_dict
    result = Stemformatics_Dataset.get_ds_ids_for_dataset_search(uid,search,filter_dict)
    return result


def test_get_ds_ids():
    temp.uid = 0
    temp.search = "ipsc and human"
    rohart_msc = False
    show_limited = False
    temp.filter_dict = {'show_limited':show_limited,'rohart_msc_test':rohart_msc} 
    result = get_ds_ids_for_dataset_search(temp)

    assert 2000 not in result
    assert 5027  in result
    assert 5016  in result
    assert len(result) > 10
 

    temp.uid = 0
    temp.search = "ipsc and musculus"
    rohart_msc = True
    show_limited = False
    temp.filter_dict = {'show_limited':show_limited,'rohart_msc_test':rohart_msc} 
    result = get_ds_ids_for_dataset_search(temp)

    assert 2000 not in result
    assert 5027 not in result
    assert 5016 not in result
    assert len(result) == 0
 
    temp.uid = 0
    temp.search = "jjjjjj"
    rohart_msc = False
    show_limited = False
    temp.filter_dict = {'show_limited':show_limited,'rohart_msc_test':rohart_msc} 
    result = get_ds_ids_for_dataset_search(temp)

    assert 2000 not in result
    assert 5027 not in result
    assert 5016 not in result
    assert len(result) == 0



    temp.uid = 3 # admin account
    temp.search = "" # this should get all of them
    rohart_msc = False
    show_limited = False
    temp.filter_dict = {'show_limited':show_limited,'rohart_msc_test':rohart_msc} 
    result = get_ds_ids_for_dataset_search(temp)

    assert 2000 in result
    assert 5027 in result
    assert 5016 in result
    assert len(result) >= 240 
 
 
    temp.uid = 0
    temp.search = None
    rohart_msc = False
    show_limited = False
    temp.filter_dict = {'show_limited':show_limited,'rohart_msc_test':rohart_msc} 
    result = get_ds_ids_for_dataset_search(temp)

    assert 2000 not in result
    assert 5027 not in result
    assert 5016 not in result
    assert len(result) == 0
 

def test_datasets_summary():
    ds_id = 2000
    dataset_status = 'Available'
    format_type = 'front_end'

    dict_of_ds_ids = {}
    dict_of_ds_ids[ds_id] = {'dataset_status':dataset_status}
    dataset = Stemformatics_Dataset.get_dataset_metadata(dict_of_ds_ids,format_type)
    assert len(dataset) == 1
    assert ds_id in dataset
    assert 'breakDown' in dataset[ds_id]
    assert 'top_diff_exp_genes' in dataset[ds_id]
    assert len(dataset[ds_id]['breakDown']) == 46
    assert dataset[ds_id]['breakDown']['Gender: female'] == 10
    assert dataset[ds_id]['top_diff_exp_genes']['MXRA5']['ensemblID'] == 'ENSG00000101825'
 

    ds_id = 4000
    dataset_status = 'Available'
    format_type = 'front_end'

    dict_of_ds_ids = {}
    dict_of_ds_ids[ds_id] = {'dataset_status':dataset_status}
    dataset = Stemformatics_Dataset.get_dataset_metadata(dict_of_ds_ids,format_type)
    assert len(dataset) == 1
    assert ds_id in dataset
    assert 'breakDown' in dataset[ds_id]
    assert 'top_diff_exp_genes' in dataset[ds_id]
    assert len(dataset[ds_id]['breakDown']) == 11
    assert dataset[ds_id]['breakDown']['Gender: female'] == 14
    assert dataset[ds_id]['top_diff_exp_genes']['Lgr5']['ensemblID'] == 'ENSMUSG00000020140'
 


def datasets_search(temp):
    uid = temp.uid
    search = temp.search
    rohart_msc = temp.rohart_msc
    filter_dict = {'show_limited':temp.show_limited,'rohart_msc_test':rohart_msc} 
    format_type = temp.format_type
    result = Stemformatics_Dataset.dataset_search(uid,search,filter_dict,format_type)

    return result

def test_datasets_for_choose_dataset():
    temp.uid = 0
    temp.search = "ipsc and human"
    temp.rohart_msc = False
    temp.show_limited = False
    temp.format_type = 'choose_dataset'
    result = datasets_search(temp)
    assert 2000 not in result
    assert len(result) > 10
    assert result[5027]['handle'] == 'Maherali_2008_18786420'
    assert result[5016]['cells_samples_assayed'] == 'hESC, iPSC, derivatives of 3-day endodermal induction of hESC and iPSC'
    assert 'breakDown' not in result[5016]

    temp.uid = 0
    temp.search = "ipsc and musculus"
    temp.rohart_msc = False
    temp.show_limited = False
    temp.format_type = 'choose_dataset'
    result = datasets_search(temp)
    assert 2000 not in result
    assert len(result) > 0
    assert result[6080]['handle'] == 'Stadtfeld_2012_22387999'
    assert result[6080]['cells_samples_assayed'] == 'iPSC'

    temp.uid = 0
    temp.search = "jjjjjjjjj"
    temp.show_limited = False
    temp.rohart_msc = False
    temp.format_type = 'choose_dataset'
    result = datasets_search(temp)
    assert 2000 not in result
    assert len(result) == 0

    temp.uid = 0
    temp.search = ""
    temp.show_limited = False
    temp.rohart_msc = False
    temp.format_type = 'choose_dataset'
    result = datasets_search(temp)
    assert 2000 in result
    assert 4000 in result
    assert len(result) > 100

    temp.uid = 0
    temp.search = "ipsc and musculus"
    temp.show_limited = False
    temp.rohart_msc = True
    temp.format_type = 'choose_dataset'
    result = datasets_search(temp)
    assert 2000 not in result
    assert len(result) == 0


def test_json():

    temp_data = {}
    temp.uid = 0
    temp.search = "ipsc and musculus"
    temp.show_limited = False
    temp.rohart_msc = False
    temp.format_type = 'choose_dataset'
    datasets = datasets_search(temp)
    temp_data['order_of_datasets'] = {}

    data = {}

    for ds_id in datasets:
        temp_dict = {}
        temp_dict['organism'] = datasets[ds_id]['organism']
        temp_dict['name'] = datasets[ds_id]['handle']
        temp_dict['title'] = datasets[ds_id]['title']
        temp_dict['cells_samples_assayed'] = datasets[ds_id]['cells_samples_assayed']
               
        data[ds_id] = temp_dict
           
    json_data = json.dumps(data)
    assert len(json_data) > 100


def test_datasets_for_export():
    temp.uid = 0
    temp.search = "ipsc and human"
    temp.rohart_msc = False
    temp.show_limited = False
    temp.format_type = 'all'
    result = datasets_search(temp)
    assert 2000 not in result
    assert len(result) > 10
    assert result[5027]['handle'] == 'Maherali_2008_18786420'
    assert result[5016]['cells_samples_assayed'] == 'hESC, iPSC, derivatives of 3-day endodermal induction of hESC and iPSC'



    temp.uid = 0
    temp.search = "ipsc and musculus"
    temp.rohart_msc = False
    temp.show_limited = False
    temp.format_type = 'front_end'
    result = datasets_search(temp)
    assert 2000 not in result
    assert len(result) > 0
    assert result[6080]['handle'] == 'Stadtfeld_2012_22387999'
    assert result[6080]['cells_samples_assayed'] == 'iPSC'

    temp.uid = 0
    temp.search = "jjjjjjjjj"
    temp.show_limited = False
    temp.rohart_msc = False
    temp.format_type = 'front_end'
    result = datasets_search(temp)
    assert 2000 not in result
    assert len(result) == 0

    temp.uid = 0
    temp.search = ""
    temp.show_limited = False
    temp.rohart_msc = False
    temp.format_type = 'front_end'
    result = datasets_search(temp)
    assert 2000 in result
    assert 4000 in result
    assert len(result) > 100

    temp.uid = 0
    temp.search = "ipsc and musculus"
    temp.show_limited = False
    temp.rohart_msc = True
    temp.format_type = 'front_end'
    result = datasets_search(temp)
    assert 2000 not in result
    assert len(result) == 0


