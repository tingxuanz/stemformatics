from guide.model.stemformatics import *
from guide.templates import *

from pylons import config


uid = 3
class tempData(object):
    pass


def test_get_all_group_names():
    expected_result = {0: u'Public', 1: u'Project Grandiose', 2: u'Nilsson', 3: u'PG_Reviewer', 4: u'MSC Signature', 6: u'Murdoch Childrens Research Institute'}
    result = Stemformatics_Auth.get_all_group_names(db)
    assert result[0] == expected_result [0]
    assert result[1] == expected_result [1]
    assert result[4] == expected_result [4]



def get_configs(temp):
    config_type = temp.config_type
    uid = temp.uid
    role = temp.role
    expected_length = temp.expected_length
    expected_types = temp.expected_types
    result = Stemformatics_Auth.get_all_group_configs(config_type,uid,role)
    assert len(result) >= expected_length
    for row in result:
        config_type = row['config_type']
        assert config_type in expected_types

def test_examples_get_configs():
    temp = tempData()
    temp.config_type = 'Annotation' 
    temp.uid = 3 #admin
    temp.role = 'admin'
    temp.expected_length = 1
    temp.expected_types = ['Annotation']
    get_configs(temp)

    temp = tempData()
    temp.config_type = 'UCSC Links' #admin
    temp.uid = 3 #admin
    temp.role = 'admin'
    temp.expected_length = 5
    temp.expected_types = ['UCSC Links']
    get_configs(temp)


    temp = tempData()
    temp.config_type = 'Links' #admin
    temp.uid = 3 #admin
    temp.role = 'admin'
    temp.expected_length = 0
    temp.expected_types = []
    get_configs(temp)

    temp = tempData()
    temp.config_type = 'UCSC Links' #admin
    temp.uid = 22 #annotator
    temp.role = 'annotator'
    temp.expected_length = 5
    temp.expected_types = ['UCSC Links']
    get_configs(temp)


    temp = tempData()
    temp.config_type = 23322
    temp.uid = 22 #annotator
    temp.role = 'annotator'
    temp.expected_length = 0
    temp.expected_types = []
    get_configs(temp)

    temp = tempData()
    temp.config_type = None
    temp.uid = 22 #annotator
    temp.role = 'annotator'
    temp.expected_length = 5
    temp.expected_types = ['UCSC Links','Annotation']
    get_configs(temp)

    temp = tempData()
    temp.config_type = None
    temp.uid = 44 #guest
    temp.role = 'normal'
    temp.expected_length = 3
    temp.expected_types = ['UCSC Links','Annotation']
    get_configs(temp)

    temp = tempData()
    temp.config_type = None
    temp.uid = 0 #guest
    temp.role = 'normal'
    temp.expected_length = 3
    temp.expected_types = ['UCSC Links','Annotation']
    get_configs(temp)




def get_groups_for_uid(temp):
    uid = temp.uid
    role = temp.role
    expected_result = temp.expected_result
    result = Stemformatics_Auth.get_groups_for_uid(db,uid,role)
    assert result == expected_result

def test_examples_get_groups_for_uid():
    temp = tempData()
    temp.uid = 3 #admin
    temp.role = u'admin'
    temp.expected_result = [0,1,2,3,4,6,7]
    get_groups_for_uid(temp)

    temp = tempData()
    temp.uid = 3 #admin
    temp.role = 'admin'
    temp.expected_result = [0,1,2,3,4,6,7]
    get_groups_for_uid(temp)

    temp = tempData()
    temp.uid = 22 
    temp.role = u'annotator'
    temp.expected_result = [0,1,2,3,4,6,7]
    get_groups_for_uid(temp)



    temp = tempData()
    temp.uid = 22 
    temp.role = 'annotator'
    temp.expected_result = [0,1,2,3,4,6,7]
    get_groups_for_uid(temp)


    temp = tempData()
    temp.uid = 44
    temp.role = 'normal'
    temp.expected_result = [0]
    get_groups_for_uid(temp)

    temp = tempData()
    temp.uid = 'x' #admin
    temp.role = 'admin'
    temp.expected_result = [0]
    get_groups_for_uid(temp)


    temp = tempData()
    temp.uid = None #admin
    temp.role = 'admin'
    temp.expected_result = [0]
    get_groups_for_uid(temp)

    temp = tempData()
    temp.uid = 0 #admin
    temp.role = 'admin'
    temp.expected_result = [0]
    get_groups_for_uid(temp)




def test_examples_get_user_role():
    temp = tempData()
    temp.uid = 3
    temp.expected_result = 'admin'
    check_get_user_role(temp)

    temp = tempData()
    temp.uid = 22 # emason annotator
    temp.expected_result = 'annotator'
    check_get_user_role(temp)



    temp = tempData()
    temp.uid = 44
    temp.expected_result = 'normal'
    check_get_user_role(temp)

    temp = tempData()
    temp.uid = 0
    temp.expected_result = None
    check_get_user_role(temp)

    temp = tempData()
    temp.uid = 0
    temp.expected_result = None
    check_get_user_role(temp)


    temp = tempData()
    temp.uid = None
    temp.expected_result = None
    check_get_user_role(temp)


    temp = tempData()
    temp.uid = 'abc'
    temp.expected_result = None
    check_get_user_role(temp)

def test_examples_for_uid_in_group():

    temp = tempData()
    temp.uid = 3
    temp.gid = 0
    temp.role = 'admin'
    temp.expected_result = True
    check_uid_in_group(temp)

    temp = tempData()
    temp.uid = 3
    temp.gid = 'abc' 
    temp.role = 'admin'
    temp.expected_result = False
    check_uid_in_group(temp)


    temp = tempData()
    temp.uid = 3
    temp.gid = 3 # not actually in this group, just because I'm admin
    temp.role = 'admin'
    temp.expected_result = True
    check_uid_in_group(temp)

    temp = tempData()
    temp.uid = 22
    temp.gid = 3 # not in group
    temp.role = 'annotator'
    temp.expected_result = True
    check_uid_in_group(temp)




    temp = tempData()
    temp.uid = 0
    temp.gid = 0
    temp.role = 'normal'
    temp.expected_result = True
    check_uid_in_group(temp)

    temp = tempData()
    temp.uid = 3
    temp.gid = 1
    temp.role = 'admin'
    temp.expected_result = True
    check_uid_in_group(temp)

    temp = tempData()
    temp.uid = 0
    temp.gid = 1
    temp.role = 'normal'
    temp.expected_result = False
    check_uid_in_group(temp)

    temp = tempData()
    temp.uid = None
    temp.gid = None 
    temp.role = None
    temp.expected_result = False
    check_uid_in_group(temp)

    temp = tempData()
    temp.uid = None
    temp.gid = 4 
    temp.role = None
    temp.expected_result = False
    check_uid_in_group(temp)

    temp = tempData()
    temp.uid = None
    temp.gid = 0 
    temp.role = None
    temp.expected_result = True
    check_uid_in_group(temp)


    temp = tempData()
    temp.uid = None
    temp.gid = 1 
    temp.role = None
    temp.expected_result = False
    check_uid_in_group(temp)


    temp = tempData()
    temp.uid = 'abc'
    temp.gid = 1 
    temp.role = None
    temp.expected_result = False
    check_uid_in_group(temp)




    result = Stemformatics_Auth.get_gid_by_name('Project Grandiose')
    assert result == 1

    result = Stemformatics_Auth.get_gid_by_name('Nothing')
    assert result == False

    result = Stemformatics_Auth.get_gid_by_name(None)
    assert result == False

    result = Stemformatics_Auth.get_gid_by_name(2321312312)
    assert result == False
    




def check_uid_in_group(temp):
    uid = temp.uid 
    gid = temp.gid
    role = temp.role
    expected_result = temp.expected_result
    result = Stemformatics_Auth.check_uid_in_group(uid,gid,role)
    assert result == expected_result 

def check_get_user_role(temp):
    uid = temp.uid 
    expected_result = temp.expected_result
    result = Stemformatics_Auth.get_user_role(db,uid)
    assert result == expected_result 



