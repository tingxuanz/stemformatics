import logging
log = logging.getLogger(__name__)


import sqlalchemy as SA
from sqlalchemy import or_, and_, desc

import re
import string
import json

from guide.model.stemformatics import *

from guide.lib.state import *

useSqlSoup = True

log.debug('Testing in guide/model/stemformatics/test_stemformatics_gene_set.py')

human_db_id = 56
mouse_db_id = 46
gene_set_description_default = "THis is a test"

probe_default = ""
gene_default = ""

def test_return_all_analysis():
    resultData =  Stemformatics_Job.return_all_analysis() 
    print resultData
    assert resultData == {0: {'name': 'Hierarchical Cluster', 'description': 'Hierarchical clustering groups genes and samples to highlight co-regulated gene sets.'}, 1: {'name': 'Comparative Marker Selection', 'Comparative marker selection shows you which of your genes of interest are most differentially expressed in distinct phenotypes within a study.': 'CMS'}, 2: {'name': 'Gene Neighbourhood', 'This analysis will find genes that share a similar expression profile for your gene of interest across samples within a given study.': 'CMS'}, 4: {'This will annotate your gene set.': 'GSA', 'name': 'Gene Set Annotation'}, 5: {'This will view your fold change.': 'FC', 'name': 'Fold Change Viewer'}, 6: {'Download the expression profile for a gene set and dataset.': 'DGS', 'name': 'Download Gene Set Expression Profile'}}



def test_return_all_status():
    resultData =  Stemformatics_Job.return_all_status() 
    print resultData
    assert resultData == {0: 'Pending', 1: 'Finished', 2: 'Error'}



def test_create_job_hierarchical_cluster_and_get_jobs_for_user():
    
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name ,'send_email_marketing': True,'send_email_job_notifications': True}
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    # print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    # print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
        
    uid = new_user.uid
    gene_set_name = "Booger"
    db_id = human_db_id
    list_of_genes = ['ENSG00000132613','ENSG00000143322','ENSG00000139645']
    
    returnData = Stemformatics_Gene_Set.addGeneSet(db,uid,gene_set_name,gene_set_description_default,db_id,list_of_genes)
    
    # print returnData
    # assert returnData == True

    checkData = Stemformatics_Gene_Set.getGeneSets(db,uid)
    
    # print checkData[0]
    gene_set_id = checkData[0].id
    assert checkData[0].gene_set_name.strip() == 'Booger'
    assert checkData[0].db_id == human_db_id
    
    
    getData = Stemformatics_Gene_Set.getGeneSetData(db,uid,checkData[0].id)
    
    print getData[1]
    assert getData[1][0].gene_id.strip() == 'ENSG00000132613'
    assert getData[1][2].gene_id.strip() == 'ENSG00000143322'
    assert getData[1][1].gene_id == 'ENSG00000139645'
    
    assert returnData == gene_set_id 
    
    analysis = 0 # HC
    use_cls = False
    use_gct = True
    dataset_id = 2000
    

    job_details = { 'analysis': analysis, 'status': 0, 'dataset_id': dataset_id, 'gene_set_id': gene_set_id, 'uid': uid, 'use_cls': use_cls, 'use_gct': use_gct, 'probe': probe_default, 'gene': gene_default}
    
    job_id = Stemformatics_Job.create_job(db,job_details)

    resultData = Stemformatics_Job.get_jobs_for_user(db,uid)

    print job_id
    print resultData
    assert job_id == resultData[0].job_id
    
    
    # Failure
    analysis = 'rowland'
    
    job_details = { 'analysis': analysis, 'status': 0, 'dataset_id': dataset_id, 'gene_set_id': gene_set_id, 'uid': uid, 'use_cls': use_cls, 'use_gct': use_gct, 'probe': probe_default, 'gene': gene_default}
    
    job_id = Stemformatics_Job.create_job(db,job_details)

    resultData = Stemformatics_Job.get_jobs_for_user(db,uid)

    print job_id
    print resultData
    assert job_id == None
    
    
    
def test_create_job_hierarchical_cluster_and_get_jobs_for_user():
    
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name ,'send_email_marketing': True,'send_email_job_notifications': True}
    db.schema = 'stemformatics' 
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    # print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    # print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
        
    uid = new_user.uid
    gene_set_name = "Booger"
    db_id = human_db_id
    list_of_genes = ['ENSG00000132613','ENSG00000143322','ENSG00000139645']
    
    returnData = Stemformatics_Gene_Set.addGeneSet(db,uid,gene_set_name,gene_set_description_default,db_id,list_of_genes)
    
    # print returnData
    # assert returnData == True

    checkData = Stemformatics_Gene_Set.getGeneSets(db,uid)
    
    # print checkData[0]
    gene_set_id = checkData[0].id
    assert checkData[0].gene_set_name.strip() == 'Booger'
    assert checkData[0].db_id == human_db_id
    
    
    getData = Stemformatics_Gene_Set.getGeneSetData(db,uid,checkData[0].id)
    
    print getData[1]
    assert getData[1][0].gene_id.strip() == 'ENSG00000132613'
    assert getData[1][2].gene_id.strip() == 'ENSG00000143322'
    assert getData[1][1].gene_id == 'ENSG00000139645'
    
    assert returnData == gene_set_id 
    
    analysis = 0 # HC
    use_cls = False
    use_gct = True
    dataset_id = 2000
    

    job_details = { 'analysis': analysis, 'status': 0, 'dataset_id': dataset_id, 'gene_set_id': gene_set_id, 'uid': uid, 'use_cls': use_cls, 'use_gct': use_gct, 'probe': probe_default, 'gene': gene_default}
    
    job_id = Stemformatics_Job.create_job(db,job_details)

    resultData = Stemformatics_Job.get_jobs_for_user(db,uid)

    print job_id
    print resultData
    assert job_id == resultData[0].job_id
    
    
    resultData = Stemformatics_Job.delete_job(db,job_id,uid)
    
    print resultData
    assert resultData == 1

    resultData = Stemformatics_Job.get_jobs_for_user(db,uid)

    print job_id
    print resultData
    assert resultData == []
    


    
    
def test_get_job_details():
    
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name ,'send_email_marketing': True,'send_email_job_notifications': True}
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    # print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    # print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
        
    uid = new_user.uid
    gene_set_name = "Booger"
    db_id = human_db_id
    list_of_genes = ['ENSG00000132613','ENSG00000143322','ENSG00000139645']
    
    returnData = Stemformatics_Gene_Set.addGeneSet(db,uid,gene_set_name,gene_set_description_default,db_id,list_of_genes)
    
    # print returnData
    # assert returnData == True

    checkData = Stemformatics_Gene_Set.getGeneSets(db,uid)
    
    # print checkData[0]
    gene_set_id = checkData[0].id
    assert checkData[0].gene_set_name.strip() == 'Booger'
    assert checkData[0].db_id == human_db_id
    
    
    getData = Stemformatics_Gene_Set.getGeneSetData(db,uid,checkData[0].id)
    
    print getData[1]
    assert getData[1][0].gene_id.strip() == 'ENSG00000132613'
    assert getData[1][2].gene_id.strip() == 'ENSG00000143322'
    assert getData[1][1].gene_id == 'ENSG00000139645'
    
    assert returnData == gene_set_id 
    
    analysis = 0 # HC
    use_cls = False
    use_gct = True
    dataset_id = 2000
    

    job_details = { 'analysis': analysis, 'status': 0, 'dataset_id': dataset_id, 'gene_set_id': gene_set_id, 'uid': uid, 'use_cls': use_cls, 'use_gct': use_gct, 'probe': probe_default, 'gene': gene_default}
    
    job_id = Stemformatics_Job.create_job(db,job_details)

    resultData = Stemformatics_Job.get_jobs_for_user(db,uid)

    # print job_id
    # print resultData
    assert job_id == resultData[0].job_id

    resultData = Stemformatics_Job.get_job_details(db,job_id)
    
    print resultData
    
    assert resultData.job_id == job_id
    assert resultData.dataset_id == dataset_id
    assert resultData.status == 0
    
    # negative
    job_id = 0 
    
    resultData = Stemformatics_Job.get_job_details(db,job_id)
    
    print resultData
    
    assert resultData == None
    
    
def test_get_job_details_check_user():
    
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name ,'send_email_marketing': True,'send_email_job_notifications': True}
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    # print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    # print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
        
    uid = new_user.uid
    gene_set_name = "Booger"
    db_id = human_db_id
    list_of_genes = ['ENSG00000132613','ENSG00000143322','ENSG00000139645']
    
    returnData = Stemformatics_Gene_Set.addGeneSet(db,uid,gene_set_name,gene_set_description_default,db_id,list_of_genes)
    
    # print returnData
    # assert returnData == True

    checkData = Stemformatics_Gene_Set.getGeneSets(db,uid)
    
    # print checkData[0]
    gene_set_id = checkData[0].id
    assert checkData[0].gene_set_name.strip() == 'Booger'
    assert checkData[0].db_id == human_db_id
    
    
    getData = Stemformatics_Gene_Set.getGeneSetData(db,uid,checkData[0].id)
    
    print getData[1]
    assert getData[1][0].gene_id.strip() == 'ENSG00000132613'
    assert getData[1][2].gene_id.strip() == 'ENSG00000143322'
    assert getData[1][1].gene_id == 'ENSG00000139645'
    
    assert returnData == gene_set_id 
    
    analysis = 0 # HC
    use_cls = False
    use_gct = True
    dataset_id = 2000
    

    job_details = { 'analysis': analysis, 'status': 0, 'dataset_id': dataset_id, 'gene_set_id': gene_set_id, 'uid': uid, 'use_cls': use_cls, 'use_gct': use_gct, 'probe': probe_default, 'gene': gene_default}
    
    job_id = Stemformatics_Job.create_job(db,job_details)

    resultData = Stemformatics_Job.get_jobs_for_user(db,uid)

    # print job_id
    # print resultData
    assert job_id == resultData[0].job_id

    resultData = Stemformatics_Job.get_job_details_check_user(db,job_id,uid)
    
    print resultData
    
    assert resultData.job_id == job_id
    assert resultData.dataset_id == dataset_id
    assert resultData.status == 0
    
    uid = 0
    
    resultData = Stemformatics_Job.get_job_details_check_user(db,job_id,uid)
    
    print resultData
    
    assert resultData == None
    
    
def test_get_user_from_job_id():
    
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name ,'send_email_marketing': True,'send_email_job_notifications': True}
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    # print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    # print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
        
    uid = new_user.uid
    gene_set_name = "Booger"
    db_id = human_db_id
    list_of_genes = ['ENSG00000132613','ENSG00000143322','ENSG00000139645']
    
    returnData = Stemformatics_Gene_Set.addGeneSet(db,uid,gene_set_name,gene_set_description_default,db_id,list_of_genes)
    
    # print returnData
    # assert returnData == True

    checkData = Stemformatics_Gene_Set.getGeneSets(db,uid)
    
    # print checkData[0]
    gene_set_id = checkData[0].id
    assert checkData[0].gene_set_name.strip() == 'Booger'
    assert checkData[0].db_id == human_db_id
    
    
    getData = Stemformatics_Gene_Set.getGeneSetData(db,uid,checkData[0].id)
    
    # print getData[1]
    assert getData[1][0].gene_id.strip() == 'ENSG00000132613'
    
    assert returnData == gene_set_id 
    
    analysis = 0 # HC
    use_cls = False
    use_gct = True
    dataset_id = 2000
    

    job_details = { 'analysis': analysis, 'status': 0, 'dataset_id': dataset_id, 'gene_set_id': gene_set_id, 'uid': uid, 'use_cls': use_cls, 'use_gct': use_gct, 'probe': probe_default, 'gene': gene_default}
    
    job_id = Stemformatics_Job.create_job(db,job_details)
    
    print job_id

    resultData = Stemformatics_Job.get_jobs_for_user(db,uid)

    # print job_id
    print resultData
    assert job_id == resultData[0].job_id

    resultData = Stemformatics_Job.get_user_from_job_id(db,job_id)

    print resultData
    assert resultData.uid == uid
    
    # failure 
    job_id = 0
    
    resultData = Stemformatics_Job.get_user_from_job_id(db,job_id)

    print resultData
    assert resultData == None



def test_create_job_comparative_marker_selection_and_get_jobs_for_user():
    
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name ,'send_email_marketing': True,'send_email_job_notifications': True}
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    # print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    # print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
        
    uid = new_user.uid
    gene_set_name = "Booger"
    db_id = human_db_id
    list_of_genes = ['ENSG00000132613','ENSG00000143322','ENSG00000139645']
    
    returnData = Stemformatics_Gene_Set.addGeneSet(db,uid,gene_set_name,gene_set_description_default,db_id,list_of_genes)
    
    # print returnData
    # assert returnData == True

    checkData = Stemformatics_Gene_Set.getGeneSets(db,uid)
    
    # print checkData[0]
    gene_set_id = checkData[0].id
    assert checkData[0].gene_set_name.strip() == 'Booger'
    assert checkData[0].db_id == human_db_id
    
    
    getData = Stemformatics_Gene_Set.getGeneSetData(db,uid,checkData[0].id)
    
    print getData[1]
    assert getData[1][0].gene_id.strip() == 'ENSG00000132613'
    assert getData[1][2].gene_id.strip() == 'ENSG00000143322'
    assert getData[1][1].gene_id == 'ENSG00000139645'
    
    assert returnData == gene_set_id 
    
    analysis = 1 # CMS
    use_cls = True
    use_gct = True
    dataset_id = 2000
    comparison_type = unicode("Disease State")

    job_details = { 'analysis': analysis, 'status': 0, 'dataset_id': dataset_id, 'gene_set_id': gene_set_id, 'uid': uid, 'use_cls': use_cls, 'use_gct': use_gct, 'probe': probe_default, 'gene': gene_default,'comparison_type': comparison_type}
    
    job_id = Stemformatics_Job.create_job(db,job_details)

    resultData = Stemformatics_Job.get_jobs_for_user(db,uid)

    print job_id
    print resultData
    assert job_id == resultData[0].job_id
    assert analysis == resultData[0].analysis
    
    
    resultData = Stemformatics_Job.delete_job(db,job_id,uid)
    
    print resultData
    assert resultData == 1

    resultData = Stemformatics_Job.get_jobs_for_user(db,uid)

    print job_id
    print resultData
    assert resultData == []
    
