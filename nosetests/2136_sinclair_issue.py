import logging
log = logging.getLogger(__name__)

from guide.model.graphs import *

import sqlalchemy as SA
from sqlalchemy import or_, and_, desc

import re
import string
import json

from guide.model.stemformatics import *

from guide.lib.state import *

useSqlSoup = True

log.debug('Testing in guide/model/stemformatics/test_stemformatics.py')
class tempData(object):
    pass

species_dict = Stemformatics_Gene.get_species(db)


def test_result_data_slpi_4000_uncollapsed():

    gene_set_id = 3083
    db_id = 56
    ds_id = 6541
    #ds_id = 2000
    #ds_id = 6461
    graphType = 'default'
    comparison_type ='Sample Type'


    _temp = tempData()



    _temp.ref_type = "gene_set_id"
    _temp.ref_id = gene_set_id
    _temp.probeSearch = ""
    _temp.geneSearch = ""
    select_probes = _temp.select_probes  = None
    _temp.db_id = db_id
    _temp.line_graph_available = False
    _temp.line_graph_available = Stemformatics_Dataset.check_line_graph_for_dataset(db,ds_id)
    graphType = Stemformatics_Dataset.check_graphType_for_dataset(db,ds_id,graphType,_temp.line_graph_available)
    _temp.graphType = graphType
    _temp.sortBy = comparison_type
    _temp.ds_id = ds_id
    _temp.choose_dataset_immediately  = False
    _temp.url = ''

    _temp.original_temp_datasets = None
    _temp.force_choose = None

    _temp.large = None
    chip_type = 199
    result = Stemformatics_Gene_Set.get_probes_from_gene_set_id(db,db_id,chip_type,gene_set_id)
    #result = get_probes_from_gene_set_id(db,db_id,chip_type,gene_set_id)
    assert result == [[u'ILMN_1690105', u'ILMN_1691364', u'ILMN_1777325', u'ILMN_1771664'], {u'ILMN_1777325': u'STAT1', u'ILMN_1771664': u'CLEC4E', u'ILMN_1690105': u'STAT1', u'ILMN_1691364': u'STAT1'}]
    #_temp = _check_dataset_status(_temp)

    #this_view = _setup_graphs(_temp)
    #self._set_outputs_for_graph()



def _check_dataset_status(_temp):
    uid = 3
    dataset_status = Stemformatics_Dataset.check_dataset_with_limitations(db,_temp.ds_id,uid)

    # if no access and already logged in then error out
    # if no access and not already logged in then redirect
    if dataset_status == "Unavailable":
        if uid == '' or uid == 0:
            # got this code from decorator in model/stemformatics/stemformatics_auth.py
            c.user = None
            session['path_before_login'] = request.path_info + '?' + request.query_string
            session.save()
            redirect(h.url('/auth/login'))
        else:
            redirect(url(controller='contents', action='index'), code=404)
    _temp.dataset_status = dataset_status

    return _temp


def _setup_graphs(temp_object):
    """ What other values are needed to be setup here for it to work?
    From expressions.py / _get_inputs_for_graph()
        self._temp.line_graph_available = Stemformatics_Dataset.check_line_graph_for_dataset(db,ds_id)
        self._temp.feature_type = feature_type
        self._temp.feature_id = feature_id
        self._temp.probeSearch = probeSearch
        self._temp.geneSearch = geneSearch
        self._temp.db_id = db_id
        self._temp.graphType = graphType
        self._temp.sortBy = sortBy
        self._temp.ds_id = ds_id
        self._temp.choose_dataset_immediately  = choose_dataset_immediately
        self._temp.url = request.environ.get('PATH_INFO')
        self._temp.original_temp_datasets = original_temp_datasets
        self._temp.force_choose = force_choose

        if request.environ.get('QUERY_STRING'):
            self._temp.url += '?' + request.environ['QUERY_STRING']
        self._temp.large = request.params.get('size') == "large"

    Note that lib/base.py / _check_dataset_status() and _check_gene_status
    are only affecting self._temp.db_id and temp_object.ref_id

    """

    ref_type = temp_object.ref_type
    ref_id = temp_object.ref_id
    graphType = temp_object.graphType
    sortBy = temp_object.sortBy
    if hasattr(temp_object,'select_probes'):
        select_probes = temp_object.select_probes
    else:
        select_probes = None

    list_of_samples_to_remove = []
    line_graph_available = temp_object.line_graph_available

    """ Build the graph data first using the temp_object and other information. And then choose the 
    graph that is appropriate and then convert the data to be ready for the view """
    this_graph_data = Graph_Data(db,temp_object.ds_id,ref_type,ref_id,temp_object.db_id,list_of_samples_to_remove,species_dict,select_probes)


    return "OK"

    if graphType == "scatter":
        this_graph = Scatterplot_Graph(this_graph_data,sortBy)
    if graphType == "box":
        this_graph = Box_Graph(this_graph_data,sortBy)
    if graphType =="bar":
        this_graph = Bar_Graph(this_graph_data,sortBy)
    if graphType =="line":
        this_graph = Line_Graph(this_graph_data,sortBy)

    this_view = Preview(this_graph,line_graph_available)
    return this_view


