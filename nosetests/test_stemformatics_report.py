import logging
log = logging.getLogger(__name__)


import sqlalchemy as SA
from sqlalchemy import or_, and_, desc

import re
import string
import json

from guide.model.stemformatics import *
from guide.model.tfv import *

from guide.lib.state import *

useSqlSoup = True

import datetime
import hashlib
from datetime import datetime, timedelta

""" Testing the auth model"""
delta = 30     
def test_new_users_last_30_days():
       
    delta_days = timedelta(days=delta)
    from_date = datetime.now() - delta_days
        
    result = Stemformatics_Auth.return_new_users(db,from_date)
    print len(result)
    assert result is None
    
    
    
def test_new_jobs_last_30_days():
    delta_days = timedelta(days=delta)
    from_date = datetime.now() - delta_days
        
    result = Stemformatics_Job.get_new_jobs(db,from_date)
    print len(result)
    print from_date
    assert result is None
    
    
