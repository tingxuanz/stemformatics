import logging
log = logging.getLogger(__name__)


import sqlalchemy as SA
from sqlalchemy import or_, and_, desc

import re
import string
import json

from guide.model.stemformatics import *
from guide.model.tfv import *

from guide.lib.state import *

useSqlSoup = True

import datetime
import hashlib

""" Testing the auth model"""

# -------------------------------------- Test check_password ---------------------------------------------------

def test_fake_username_for_check_user_password():
    db_user = Stemformatics_Auth.check_user_password(db,"test","booger")
    print db_user
    assert db_user is None
    
    
def test_wrong_password_for_check_user_password():
    db_user = Stemformatics_Auth.check_user_password(db,"test@mailinator.com","booger")
    print db_user
    assert db_user is None


def test_correct_check_user_password():
    db_user = Stemformatics_Auth.check_user_password(db,"test@mailinator.com","Temp55%%")
    print db_user
    assert db_user is not None
    assert db_user.username == "test@mailinator.com"
    assert db_user.status == 1
    
        

# ------------------------------------------ Register new user ---------------------------------------------------

# short password
def test_password_register_new_user_error():
    
    username = 'test@mailinator.com'
    pwd = 'booger'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    print new_user
    assert new_user == "Your password must contain at least 15 characters with at least one space"

# strong password but not long enough
def test_password_register_new_user_error2():
    
    username = 'test@mailinator.com'
    pwd = 'B1#ooge'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    
    print new_user
    assert new_user == "Your password must contain at least 15 characters with at least one space"

# strong password but too long enough
def test_password_register_new_user_error3():
    
    username = 'test@mailinator.com'
    pwd = 'B1#ooges746382654982345689346583465823465892364589236489561234589623489562349856389245689374658923465389465348956'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    
    print new_user
    assert new_user == "Your password must contain at least 15 characters with at least one space"


# invalid email address
def test_password_register_new_user_error4():
    
    username = 'test%%%@mailinator.com'
    pwd = 'B1#ooger is a new user now'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    
    print new_user
    assert new_user == "This username is not valid, it must be a valid email address"



# invalid email address
def test_password_register_new_user_error5():
    
    username = 'testmailinator.com'
    pwd = 'B1#ooger is a new person'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    
    print new_user
    assert new_user == "This username is not valid, it must be a valid email address"
    
    
# invalid email address
def test_password_register_new_user_error6():
    
    username = 'test@mailinator..com'
    pwd = 'B1#ooger is a new person'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    
    print new_user
    assert new_user == "This username is not valid, it must be a valid email address"
    

# username taken
def test_password_register_new_user_error7():
    
    username = 'test@mailinator.com'
    pwd = 'Booger is a new person'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    print new_user
    assert new_user == "This username is already taken"
    



def test_password_register_new_user_success():
    
    username = 'test2321321312312@mailinator.com'
    
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    

# ------------------------------------------------------- Confirm new user ---------------------------------------     

# wrong confirm code            
def test_confirm_new_user_error1():
    
    username = 'test2321321312312@mailinator.com'
    
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code +'99',new_user.uid)
    print result
    assert result == "This confirm code is invalid or has expired for this user"


#  make creation date more than 2 days ago
def test_confirm_new_user_error2():
    
    username = 'test2321321312312@mailinator.com'
    
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    # change created date to more than 2 days ago.
    db.schema = 'stemformatics'
    user = db.users
    
    now_time = datetime.datetime.now()
    three_days = datetime.timedelta(days=3)
    
    new_time = now_time - three_days
    
    
    result = user.filter(user.username == username).update({'created': new_time})
    
    confirm_code_new = new_user.confirm_code.strip()
    
    result = Stemformatics_Auth.confirm_new_user(db,confirm_code_new,new_user.uid)
    print result
    assert result == "This confirm code has expired for this user"
        

    
#  make user status to be active
def test_confirm_new_user_error3():
    
    username = 'test2321321312312@mailinator.com'
    
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    # change created date to more than 2 days ago.
    db.schema = 'stemformatics'
    user = db.users
    
    result = user.filter(user.username == username).update({'status': 1})
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    print result
    assert result == 'This user is already registered'
    
    


#  make user confirmed
def test_confirm_new_user_success():
    
    username = 'test2321321312312@mailinator.com'
    
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    # change created date to more than 2 days ago.
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
            


#  ----------------------------------------------------- Clear expired unconfirmed users -------------------------------------------------------
def test_clear_expired_unconfirmed_users_success():
    username = 'test2321321312312@mailinator.com'
    
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    # change created date to more than 2 days ago.
    db.schema = 'stemformatics'
    user = db.users
    
    now_time = datetime.datetime.now()
    three_days = datetime.timedelta(days=3)
    
    new_time = now_time - three_days
    
    
    result = user.filter(user.username == username).update({'created': new_time})

    result = Stemformatics_Auth.clear_expired_unconfirmed_users(db)
    print result
    assert result == True




    
#  ---------------------------------------------------------------- remove user -----------------------------------------------
def test_remove_user_success():
    
    username = 'test2321321312312@mailinator.com'
    
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    # change created date to more than 2 days ago.
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
            
    result = Stemformatics_Auth.remove_user(db,username)
    print result
    assert result == True
    

# ---------------------------------------------------------------- Password reset -------------------------------------------------

# unknown username
def test_set_confirm_forgot_password_error1():
        
    username = 'test2321321312312@mailinator.com.au'
    
    result = Stemformatics_Auth.set_confirm_forgot_password(db,username)
    print result
    assert result == None
    

# user is not active
def test_set_confirm_forgot_password_error2():
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    
    result = Stemformatics_Auth.set_confirm_forgot_password(db,username)
    print result
    assert result == None


# successfully started the process for resetting password    
def test_set_confirm_forgot_password_success():
        
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
    
    result = Stemformatics_Auth.set_confirm_forgot_password(db,username)
    print result
    assert result.username == username
    assert result.confirm_code != ""
    assert result.password_expiry is not None
    
    
# error confirming,  expired
def test_reset_password_error1():
        
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
    
    result = Stemformatics_Auth.set_confirm_forgot_password(db,username)
    print result
    assert result.username == username
    assert result.confirm_code != ""
    assert result.password_expiry is not None

    # change password_expiry date to more than 3 hours ago.
    db.schema = 'stemformatics'
    user = db.users
    
    now_time = datetime.datetime.now()
    three_hours = datetime.timedelta(hours=3)
    
    new_time = now_time - three_hours
    
    
    update_result = user.filter(user.username == username).update({'password_expiry': new_time})


    updated_user = Stemformatics_Auth.reset_password(db,result,result.confirm_code.strip(),pwd+'**')
    print updated_user
    assert updated_user == "This has expired. Please try again"
    

def test_reset_password_success():
        
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
    
    result = Stemformatics_Auth.set_confirm_forgot_password(db,username)
    print result
    assert result.username == username
    assert result.confirm_code != ""
    assert result.password_expiry is not None

    updated_user = Stemformatics_Auth.reset_password(db,result,result.confirm_code.strip(),pwd+'**')
    
    assert updated_user == 1
    
    
def test_clear_expired_password_resets_success():
        
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
    
    result = Stemformatics_Auth.set_confirm_forgot_password(db,username)
    print result
    assert result.username == username
    assert result.confirm_code != ""
    assert result.password_expiry is not None

    # change password_expiry date to more than 3 hours ago.
    db.schema = 'stemformatics'
    user = db.users
    
    now_time = datetime.datetime.now()
    three_hours = datetime.timedelta(hours=3)
    
    new_time = now_time - three_hours
    
    update_result = user.filter(user.username == username).update({'password_expiry': new_time})    
    
    result = Stemformatics_Auth.clear_expired_password_resets(db)
    print result
    assert result == True
    
    check_result = user.filter(user.username == username).one()
    
    print check_result.password_expiry
    
    assert check_result.password_expiry == None
    

# -------------------------------------------------------------------- Change password ---------------------------------------

def test_change_password_success():    
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
    

    result = Stemformatics_Auth.change_password(db,username,pwd+'6565')
    print result
    assert result.username == username
    
    m = hashlib.sha1()
    m.update(pwd+'6565')
    sha1_password = m.hexdigest()

    assert sha1_password == result.password




# "There was an error finding this user"
def test_change_password_error1():    
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
    

    result = Stemformatics_Auth.change_password(db,username+'llll',pwd+'6565')
    print result
    assert result == "There was an error finding this user"

    
    
# "Your password must contain at least 1 upper case letter, 1 lower case letter, 1 number, 1 special character (no spaces) and be 8 to 20 characters in length"
def test_change_password_error2():    
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
    

    result = Stemformatics_Auth.change_password(db,username+'llll','Blooger')
    print result
    assert result == "Your password must contain at least 15 characters with at least one space"
    


# --------------------------------------------------- Update user ---------------------------------------------------------

# "There was an error finding this user"
def test_update_user_error1():    
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
    
    password = 'Bl#oog'
    org = 'Not Griffith'
    name = 'peter '
    
    updated_data = { 'password': password, 'organisation': org, 'full_name': name }
    result = Stemformatics_Auth.update_user(db,username+'ppp',updated_data)
    print result
    assert result == "There was an error finding this user"
    
    
    
# "This password needs 1 upper case letter, 1 lower case letter, 1 number, 1 special char, 8 to 20 characters, no spaces"
def test_update_user_error2():    
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
        
    password = 'B33333'
    org = 'Not Griffith'
    name = 'peter '
    
    updated_data = { 'password': password, 'organisation': org, 'full_name': name }
    result = Stemformatics_Auth.update_user(db,username,updated_data)
    print result
    assert result == "Your password must contain at least 15 characters with at least one space"
    
    
    
def test_update_user_success():    
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    # print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    # print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
        
    password = 'Bl#oog123 another long name'
    org = 'Not Griffith'
    name = 'peter'
    
    updated_data = { 'password': password, 'organisation': org, 'full_name': name }
    result = Stemformatics_Auth.update_user(db,username,updated_data)
    print result
    assert result == True
    
    result = user.filter(user.username == username).one()
    print result
    assert result.organisation.strip() == org
    assert result.full_name.strip() == name
    
    m = hashlib.sha1()
    m.update(password)
    sha1_password = m.hexdigest()

    assert result.password == sha1_password
    


def test_get_user_from_username_success():
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
    
    result = Stemformatics_Auth.get_user_from_username(db,username)
    print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
    

def test_get_user_from_username_error():
    username = 'restingsdfsdfs@mailinator.com.au'
    
    result = Stemformatics_Auth.get_user_from_username(db,username)
    print result
    assert result is None
    
    
def test_get_user_from_confirm_code_success():
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.get_user_from_confirm_code(db,new_user.confirm_code)
    print result
    assert result.username == username
    assert result.status == 0
    assert result.confirm_code == new_user.confirm_code
    
    
def test_get_user_from_confirm_code_error():
    confirm_code = 'restingsdfsdfs@mailinator.com.au'
    result = Stemformatics_Auth.get_user_from_confirm_code(db,confirm_code)
    print result
    assert result is None

def test_get_user_from_uid_success():
    username = 'test2321321312312@mailinator.com'
    
    # setup user
    Stemformatics_Auth.remove_user(db,username)
    
    pwd = 'B1#ooger this is a test'
    org = 'Griffith'
    name = 'Testing ony'
    registration_data = { 'username': username, 'password': pwd, 'organisation': org, 'full_name': name }
    
    # return the new user record
    new_user = Stemformatics_Auth.register_new_user(db,registration_data)
    print new_user
    assert new_user.confirm_code != ""
    assert new_user.status == 0
    
    db.schema = 'stemformatics'
    user = db.users
    
    result = Stemformatics_Auth.confirm_new_user(db,new_user.confirm_code.strip(),new_user.uid)
    print result
    assert result.uid == new_user.uid
    assert result.status == 1
    assert result.confirm_code == ""
    
    result = Stemformatics_Auth.get_user_from_uid(db,result.uid)
    print result
    assert result.username == username
    assert result.status == 1
    assert result.confirm_code == ""
    
def test_get_user_from_uid_error():
    uid = 2131465432111
    
    result = Stemformatics_Auth.get_user_from_username(db,uid)
    print result
    assert result is None
