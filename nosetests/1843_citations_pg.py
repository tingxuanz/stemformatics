## NOTE: Some of the data in this this test could have been overwritten by
##	changes in Task #1810
from guide.model.stemformatics import *
from pylons import request, response, session, url, tmpl_context as c,config
import json
from guide.model.graphs import *

species_dict = Stemformatics_Gene.get_species(db)

class tempData(object):
    pass

temp = tempData() 


uid = 3

dev_test ={6128: u'Nature. 2014 Dec', 6130: u'Nature. 2014 Dec', 6131: u'Nature. 2014 Dec', 6197: u'Nature. 2014 Dec', 6198: u'Nature. 2014 Dec'}

real_6131_title = u'Project Grandiose: An epigenomic roadmap to induced pluripotency - MethylCpG'
real_6131_publication_title = u'Project Grandiose: An epigenomic roadmap to induced pluripotency'
 
def test_citations():
    ds_ids = [6197,6198,6131,6130,6128]
    result = Stemformatics_Dataset.get_citations_for_dataset_list(ds_ids)

    # this result is only test data
    assert result[6131]['Publication Title'] == real_6131_publication_title



def test_errors():
    
    ds_ids = ['a',6198,6131,6130,6128]
    result = Stemformatics_Dataset.get_citations_for_dataset_list(ds_ids)
    assert result[6131]['Publication Title'] == real_6131_publication_title


    ds_ids = ['a','6128']
    result = Stemformatics_Dataset.get_citations_for_dataset_list(ds_ids)
    assert result == {}


    ds_ids = 'a'
    result = Stemformatics_Dataset.get_citations_for_dataset_list(ds_ids)
    assert result == {}

    ds_ids = None
    result = Stemformatics_Dataset.get_citations_for_dataset_list(ds_ids)
    assert result == {}

    ds_ids = {}
    result = Stemformatics_Dataset.get_citations_for_dataset_list(ds_ids)
    assert result == {}

    ds_ids = 6128
    result = Stemformatics_Dataset.get_citations_for_dataset_list(ds_ids)
    assert result == {}

    ds_ids = 23421432134123412
    result = Stemformatics_Dataset.get_citations_for_dataset_list(ds_ids)
    assert result == {}


def test_encode_data():
    ds_id = 6131
    result = Stemformatics_Dataset.getDatasetDetails(db,ds_id,uid)
    assert 6131 in result
    assert 'publication_title' in  result[6131]
    assert 'title' in  result[6131]
    assert result[6131]['publication_title'] == real_6131_publication_title
    assert result[6131]['title'] == real_6131_title

def test_thomson_reuters():
    # NOTE cannot test this as you cannot get url to work within the model using nosetests.
    # should have a look at this when we have time
    # You have to test this module by running the page on stemformatics admin
    return 
    result = Stemformatics_Dataset.get_thomson_reuters_feed()
    assert result[5018]['Contact Email']  == 'joewu@stanford.edu'
    assert result[2000]['Year']  == '2010'
    assert result[4000]['GEO Accession']  == 'GSE20402'
    assert result[4000]['Title']  == 'Mouse mammary epithelial cell subpopulations from pregnant and virgin mice'
    assert len(result) > 100

    file_text = Stemformatics_Dataset.create_thomson_reuters_xml_file(result)
    print file_text
    assert file_text == True

def test_view_for_graph():
    temp.ds_id = 6131
    temp.db_id = 46
    temp.ensembl_id = 'ENSMUSG00000030142'
    temp.ref_type = 'ensemblID'
    temp.ref_id = temp.ensembl_id
    temp.sortBy = 'Sample Type'
    temp.list_of_samples_to_remove = [] 
    temp.line_graph_available = False

    temp.expected_chip_type = 137
    temp.expected_data_levels = 2
    temp.expected_handle ='Lee_2013_under_review_PRIVATE' 
    temp.expected_x_axis_label = 'D5H'
    temp.expected_title_id = ' for Gene Clec4e'
    temp.expected_title= real_6131_title
    temp.expected_xaxis_label_type_bar_and_box = 'Probe'
    temp.expected_disease_x_axis_label = 'Normal' 
    temp.expected_probe_list = [u'ENSMUST00000032239chr6:123235889-123239889']

    setup_graphs(temp)



def setup_graphs(temp):
    this_graph_data = Graph_Data(db,temp.ds_id,temp.ref_type,temp.ref_id,temp.db_id,temp.list_of_samples_to_remove,species_dict)
    assert this_graph_data.handle == temp.expected_handle 
    assert this_graph_data.ref_type == temp.ref_type
    assert this_graph_data.ref_id == temp.ref_id
    assert this_graph_data.db_id == temp.db_id
    assert this_graph_data.chip_type == temp.expected_chip_type


    this_graph = Scatterplot_Graph(this_graph_data,temp.sortBy) 
    assert isinstance(this_graph.plot_data,dict)
    assert temp.expected_x_axis_label in this_graph.xaxis_labels
    assert this_graph.data_levels == temp.expected_data_levels
    print this_graph.graph_data.probe_list
    assert this_graph.graph_data.probe_list == temp.expected_probe_list

    this_view = Preview(this_graph,temp.line_graph_available)
    assert this_view.title_id == temp.expected_title_id
    assert this_view.ensembl_id == temp.ensembl_id
    assert this_view.line_graph_available == False
    assert this_view.title_dataset == temp.expected_title
    assert temp.expected_x_axis_label in this_view.graph.xaxis_labels
    assert this_view.graph.data_levels == temp.expected_data_levels


    this_graph = Box_Graph(this_graph_data,temp.sortBy) 
    assert isinstance(this_graph.plot_data,dict)
    assert this_graph.xaxis_labels[0]['type'] == temp.expected_xaxis_label_type_bar_and_box
    assert this_graph.data_levels == temp.expected_data_levels

    this_view = Preview(this_graph,temp.line_graph_available)
    assert this_view.title_id == temp.expected_title_id
    assert this_view.ensembl_id == temp.ensembl_id
    assert this_view.line_graph_available == False
    assert this_view.title_dataset == temp.expected_title
    assert this_view.graph.xaxis_labels[0]['type'] == temp.expected_xaxis_label_type_bar_and_box
    assert this_view.graph.data_levels == temp.expected_data_levels


    this_graph = Bar_Graph(this_graph_data,temp.sortBy) 
    assert isinstance(this_graph.plot_data,dict)
    assert this_graph.xaxis_labels[0]['type'] == temp.expected_xaxis_label_type_bar_and_box
    assert this_graph.data_levels == temp.expected_data_levels
    assert this_view.title_dataset == temp.expected_title

    this_view = Preview(this_graph,temp.line_graph_available)
    assert this_view.title_id == temp.expected_title_id
    assert this_view.ensembl_id == temp.ensembl_id
    assert this_view.line_graph_available == False
    assert this_view.graph.xaxis_labels[0]['type'] == temp.expected_xaxis_label_type_bar_and_box
    assert this_view.title_dataset == temp.expected_title
    assert this_view.graph.data_levels == temp.expected_data_levels

    print this_view.graph.graph_data.probe_list
    assert this_view.graph.graph_data.probe_list == temp.expected_probe_list

    # disease state
    temp.sortBy = 'Disease State'
    this_graph = Scatterplot_Graph(this_graph_data,temp.sortBy) 
    assert isinstance(this_graph.plot_data,dict)
    assert temp.expected_disease_x_axis_label in this_graph.xaxis_labels
    assert this_graph.data_levels == temp.expected_data_levels

    this_view = Preview(this_graph,temp.line_graph_available)
    assert this_view.title_id == temp.expected_title_id
    assert this_view.ensembl_id == temp.ensembl_id
    assert this_view.line_graph_available == False
    assert temp.expected_disease_x_axis_label in this_view.graph.xaxis_labels
    assert this_view.graph.data_levels == temp.expected_data_levels


    # line graph - leave for now.



