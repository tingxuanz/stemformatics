import logging
from pylons import request, response, session, url, tmpl_context as c
#Mark user as logged in
from pylons.controllers.util import abort, redirect


import codecs
from sqlalchemy import or_, and_, desc
from sqlalchemy.exceptions import *

from paste.deploy.converters import asbool

import json

#for result data only
import math 

import logging
log = logging.getLogger(__name__)

# Live querying
from guide.model.stemformatics import *
import re

# Import smtplib for the actual sending function
import smtplib


from pylons import config

import os, subprocess

connection = db.engine.connect()


# for some reason it is not applied in guide/lib/base.py
import guide.lib.helpers as h

from datetime import datetime, timedelta
from matricks import *
import cPickle

from pylons import app_globals as g


class MyClass:
    i=12345

self = MyClass()
self.human_db = config['human_db']
self.mouse_db = config['mouse_db']


self.default_human_dataset = int(config['default_human_dataset'])
self.default_mouse_dataset = int(config['default_mouse_dataset'])
self.useSqlSoup = True

if 'useSqlSoup' in config:
    self.useSqlSoup = asbool(config['useSqlSoup'])

# GenePattern modules
self.GPQueue = config['GPQueue']
self.StemformaticsQueue = config['StemformaticsQueue']
self.StemformaticsController = config['StemformaticsController']
self.GeneSetFiles = config['GeneSetFiles']
self.DatasetGCTFiles = config['DatasetGCTFiles']
uid = 3
dataset_id = ds_id = 5003
gene_set_id = 569 #2900 genes
#gene_set_id = 454 #1100 genes
#gene_set_id = 471 # small one
chip_type = Stemformatics_Dataset.getChipType(db,dataset_id)
db_id = Stemformatics_Dataset.getDatabaseID(db,dataset_id,self.human_db,self.mouse_db,uid)
result = Stemformatics_Gene_Set.get_probes_from_gene_set_id(db,db_id,chip_type,gene_set_id)
new_gct_header = Stemformatics_Expression.return_gct_file_sample_headers_as_replicate_group_id(db,ds_id)
def test():

    #using_redis()
    using_gct_file()


def using_redis():
    assert True == True 
    gct_filename = '/tmp/job.gct'
    probe_list = result[0]
    probe_dict = result[1]
   
    probe_expression_rows = Stemformatics_Expression.get_expression_rows(ds_id,probe_list)
    sample_labels = Stemformatics_Expression.get_sample_labels(ds_id)
    
    returnList = {}
    count = 0
    metaDataList = {}
    number_of_rows = len(probe_expression_rows)
    number_of_samples = len(sample_labels)
    gct_text = "#1.2\n"+str(number_of_rows)+"\t"+str(number_of_samples)+"\n"+new_gct_header
    
    for row in probe_expression_rows:
        #row_dict = gene_filtered_matricks_result.todict(row)
        probe_id = row
        gene = probe_dict[probe_id]
        gct_row_text = probe_id +" " + gene+ "\t"+"na"
        sample_count = 0
        for expression_value in probe_expression_rows[row]:
            if expression_value != '' and expression_value != 'None':
                expression_value = float(expression_value) 
            else:
                expression_value = -999999999
                #expression_value = -1
            gct_row_text += "\t" + str(expression_value)

        gct_row_text += "\n"
        gct_text += gct_row_text
    gct_file = codecs.open(gct_filename,"w","utf-8")
    gct_file.write(gct_text)
    gct_file.close()

def using_gct_file():
    gct_filename = '/tmp/job1.gct'
     
    # get chip type from dataset id
    read_gct_file_name = self.DatasetGCTFiles + 'dataset'+str(dataset_id)+'.gct'
    m_dataset = Matricks(open(read_gct_file_name,'r'))
    
    probe_list = result[0] 
    
    probe_mappings = result[1] # this has probe to gene symbols
    
    gct_file = codecs.open(gct_filename,"w","utf-8")
    m_dataset.extractRows(probe_list).gct(gct_file)
    gct_file.close()
    
    # now go through gct file and convert probe ids to be gene name and probe ids

    gct_file = codecs.open(gct_filename,'r',"utf-8")
    new_lines = ""
    count = 0
    for line in gct_file:
        count +=1
        if count ==3:
            line = new_gct_header
        else:
            row_list = line.split('\t')
            probe_id = row_list[0]
            
            if probe_id in probe_mappings:
                new_id =  probe_id + ' ' + probe_mappings[probe_id]
                line = line.replace(probe_id,new_id)
        
        new_lines = new_lines + line
    
    gct_file.close()
    
    gct_file = codecs.open(gct_filename,"w","utf-8")
    gct_file.write(new_lines)
    gct_file.close()

