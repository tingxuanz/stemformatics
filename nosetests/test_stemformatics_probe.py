import logging
log = logging.getLogger(__name__)


import sqlalchemy as SA
from sqlalchemy import or_, and_, desc

import re
import string
import json

from guide.model.stemformatics import *

from guide.lib.state import *

useSqlSoup = True

import datetime
import hashlib

""" Testing the probe model"""

# -------------------------------------- Test check_password ---------------------------------------------------

def test_return_probe_information_success():
    identifier = 'ENSG00000115415'
    db_id = 56
    chip_type = 6
    result = Stemformatics_Probe.return_probe_information(db,identifier,db_id,chip_type)
    print result
    #assert result ==  {u'ILMN_1777325': 1, u'ILMN_1690105': 1, u'ILMN_1691364': 1}

    

def test_return_probe_information_error():
    identifier = 'ENSG00000115415'
    db_id = 56
    chip_type = 456565656
    result = Stemformatics_Probe.return_probe_information(db,identifier,db_id,chip_type)
    print result
    assert result == []
        
        
        
def test_get_genes_for_probe_success():
    probe_id = 'ILMN_1690105'
    db_id = 56
    chip_type = 6
    result = Stemformatics_Probe.get_genes_for_probe([probe_id],db_id,chip_type)
    print result
    assert result[0]['gene_id'] == 'ENSG00000115415'
    assert len(result) == 1

def test_get_genes_for_probe_multiple_results_success():
    probe_id = 'ILMN_1679060'
    db_id = 56
    chip_type = 7
    result = Stemformatics_Probe.get_genes_for_probe([probe_id],db_id,chip_type)
    print result
    assert result[0]['gene_id'] == 'ENSG00000204531'
    assert len(result) == 8

def test_get_genes_for_multiple_probes_multiple_results_success():
    probe_ids = ['ILMN_1679060','ILMN_1771664']
    db_id = 56
    chip_type = 7
    result = Stemformatics_Probe.get_genes_for_probe(probe_ids,db_id,chip_type)
    print result
    assert len(result) == 9
    assert result[0]['gene_id'] == 'ENSG000001665231'

     

def test_get_genes_for_probe_error():
    probe_id = 'ILMN_16901054444'
    db_id = 56
    chip_type = 456565656
    result = Stemformatics_Probe.get_genes_for_probe([probe_id],db_id,chip_type)
    print result
    assert result == []    
    
