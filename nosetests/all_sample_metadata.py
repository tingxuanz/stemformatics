from pylons import request, response, session, url, tmpl_context as c,config
import json
from guide.model.stemformatics import *



def test_1():

    file_name = config['all_sample_metadata_cpickle_file']
    f = open(file_name, 'rb')
    all_sample_metadata = json.loads(f.read())
    all_sample_metadata = all_sample_metadata
    f.close() 

    assert len(all_sample_metadata) == 27

    from_setup_all_sample_metadata = Stemformatics_Expression.setup_all_sample_metadata(db)
    assert len(from_setup_all_sample_metadata) == 27


    assert from_setup_all_sample_metadata == all_sample_metadata
