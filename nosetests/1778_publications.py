from guide.model.stemformatics import *


def test_get_all_publications():
    data_publications = Stemformatics_Dataset.get_data_publications()

    assert data_publications[1]['ds_id'] == 2000
    assert data_publications[0]['ds_id'] == 5008
    assert 5037 not in data_publications
    assert 'Publication Title' in data_publications[1]
    assert 'Authors' in data_publications[1]
    assert data_publications[1]['Publication Title'] == u'Disease-specific, neurosphere-derived cells as models for brain disorders'
    assert data_publications[1]['Authors'] == u'Nicholas Matigian, Greger Abrahamsen, Ratneswary Sutharsan, Anthony L Cook, Amanda Nouwens, Bernadette Bellette, Alejandra M Vitale, Jiyuan An, Matthew Anderson, Anthony Beckhouse, Maikel Bennebroek, Rowena Cecil, Alistair M Chalk, Julie Cochrane, Youngjun Fan, Francois Feron, Richard McCurdy, John McGrath, W. Murrell, Chris Perry, Jothy Raju, Sugandha Ravishankar, Peter Silburn, Greg Sutherland, Stephen Mahler, George Mellick, Stephen A Wood, Carolyn Sue, Christine Wells, Alan Mackay-Sim'
    assert data_publications[1]['Publication Date'] == '2010-08'
    assert data_publications[0]['Publication Date'] == '2013-08'

