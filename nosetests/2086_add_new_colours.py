
# From http://stackoverflow.com/questions/470690/how-to-automatically-generate-n-distinct-colors

import numpy as np
import colorsys

def _get_colors(num_colors):
    colors=[]
    for i in np.arange(0., 360., 360. / num_colors):
        hue = i/360.
        lightness = (50 + np.random.rand() * 10)/100.
        saturation = (90 + np.random.rand() * 10)/100.
        colors.append(colorsys.hls_to_rgb(hue, lightness, saturation))
    return colors

def test_all_colours():

    colours_needed = 100

    colour_array_hsv = _get_colors(colours_needed)

    hex_out = []
    for rgb in colour_array_hsv:
        rgb = map(lambda x: int(x*255),colorsys.hsv_to_rgb(*rgb))
        hex_out.append('#'+"".join(map(lambda x: chr(x).encode('hex'),rgb)))

    assert True == False
