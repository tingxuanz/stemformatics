#!/bin/bash
HOMEDIR=/home/rowlandm/s4m
cd $HOMEDIR/Portal/guide/public/js/flot
cat *.js | yui-compressor --type=js > s4m_graphing.min.js
ls -alh
cd ../expressions/
rm *_min.js
rm geg.min.js
yui-compressor -o '.js$:_min.js' *.js
cat graph_min.js gene_expression_graph_triggers_min.js gene_expression_graphs_min.js  > geg.min.js
ls -alh

cd $HOMEDIR/Portal/guide/public

# This needs to be used as it has the cookie consent plugin. We want to have the css being used but not the js.
# This is because that existing users can then have the css cached so that when we turn on the js later, they 
# will have a better experience
#cat js/cookieconsent.plugin.min.js js/main.js js/table2CSV.js  | yui-compressor --type=js > js/main_table2CSV_help_min.js

cat js/main.js js/table2CSV.js  | yui-compressor --type=js > js/main_table2CSV_help_min.js
ls -alh js/main_table2CSV_help_min.js


cd $HOMEDIR/Portal/guide/public/js/msc_signature
rm *_min.js
cat rohart_msc_test.js rohart_msc_graph.js | yui-compressor --type=js  > rohart_msc_test_min.js
ls -alh 
