def rgb(cmyk, base=255):
    '''
    Return RGB values of the colour as r,g,b tuple. base may be 1 or 255.
    Eg: print colourfunctions.rgb('0,0,0,25') 
    prints (191.25, 191.25, 191.25)
    This will return a tuple of numbers corresponding to r,g,b values.
    '''
    c,m,y,k = cmyk.split(',')
	# Convert cmyk to cmy first by adding black to cmy values. The black value to add is the minimum of the other 2 values
    (c,m,y,k) = (float(c), float(m), float(y), float(k))
    f = (1-k/100)/100
    cyan = c*f+k/100 if c*f+k/100<1 else 1
    magenta = m*f+k/100 if m*f+k/100<1 else 1
    yellow = y*f+k/100 if y*f+k/100<1 else 1
    return (1-cyan, 1-magenta, 1-yellow) if base==1 else ((1-cyan)*255, (1-magenta)*255, (1-yellow)*255)

def hex(rgb):
    '''
    Return RGB hex representation of the colour, eg. #666666. rgb should have base 255
    Eg: print colourfunctions.hex((191.25, 191.25, 191.25)) or
    print colourfunctions.hex(rgb('0,0,0,25'))
    Note the extra bracket in example argument: rgb is the variable pointing to the tuple
    '''
    return "#%02X%02X%02X" % rgb


