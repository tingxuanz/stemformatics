"""\
ColorMath Functions
===================

Several functions for doing RGB <--> HSV / HSL / HSI conversions
to support GuIDE & Matricks based on
algorithms found on http://en.wikipedia.org/wiki/HSV_color_space

Nick Seidenman <seidenman@wehi.edu.au>
Walter & Eliza Hall Institute
Molecular Medicine Division
Melbourne VIC
Australia

"""

int16 = lambda x: int(x, 16) if isinstance(x, (str, unicode)) else int(x)

def rgb2hsv(rgb):
    """
Convert rgb (encoded as string or seq) to hsv.


*rgb* can be specified as a string of three or six hexadecimal digits
 with an optional '#' prefix, or as a three-tuple of integer or floating
point values between 0 and 1.  Integer values will be converted to floating
point in the range [0,1]; floating point values will be used unmodified.
"""
    
    if isinstance(rgb, (str, unicode)):
        if rgb.startswith('#'): rgb = rgb[1:]
        if len(rgb) == 6:  # 6 hex digits
            vec = int16(rgb[:2]) / 255.0, int16(rgb[2:4]) / 255.0, int16(rgb[4:6]) / 255.0
        elif len(rgb) ==3: # 3 hex digits
            vec = int16(rgb[0]) / 15.0, int16(rgb[1]) / 15.0, int16(rgb[2]) / 15.0
        else:
            raise Exception('invalid color specification: ' + rgb)
        
    elif isinstance(rgb, (list, tuple)):
        vec = tuple([ ((int16(c) / 255) if int16(c) > 1 else float(c)) for c in rgb ])
        
    else:
        raise Exception('invalid color specification: ' + str(rgb))

    m, M = min(vec), max(vec)
    if m < 0 or M > 1:
        raise Exception('invalid color specification: ' + str(rgb))

    # Calculate the chroma (magnitude of projection from color cube to hexagon plane.)
    C = M - m
    
    # Calculate H'
    if C == 0:  # undefined
        Hpr = 0
        
    elif M == vec[0]:  # M == R
        Hpr = ((vec[1] - vec[2]) / C) % 6

    elif M == vec[1]:  # M == G
        Hpr = ((vec[2] - vec[0]) / C) + 2

    elif M == vec[2]:  # M == B
        Hpr = ((vec[0] - vec[1]) / C) + 4

    H = 60.0 * Hpr

    V = M  # Using the hexicone model

    S = C / V

    return (H, S, V)

def hsv2rgb(hsv):
    """\
Convert the three-tuple representing an HSV-encoded color to
RGB tuple. 
"""
    if not isinstance(hsv, (tuple, list)) or len(hsv) != 3:
        raise Exception('invalid hsv specified: ' + str(hsv))

    # A little notational suger ...
    H, S, V = hsv

    # First, find the chroma
    C = V * S

    # Compute H'
    Hpr = H / 60.0

    # (compensates for the mod 360 thing, which makes 0xff the same as 0x00.)
    if H == 0.0: H = 6.0

    # Compute intermediate value for second largest component of color
    X = C * (1 - abs((Hpr % 2) - 1))

    # Scaling factor, used later.

    m = V - C
    # Compute R1, G1, B1
    if H is None or H == 0:
        rgb1 = (0, 0, 0)

    elif Hpr < 1:
        rgb1 = (C, X, 0)

    elif Hpr < 2:
        rgb1 = (X, C, 0)

    elif Hpr < 3:
        rgb1 = (0, C, X)

    elif Hpr < 4:
        rgb1 = (0, X, C)

    elif Hpr < 5:
        rgb1 = (X, 0, C)

    elif Hpr < 6:
        rgb1 = (C, 0, X)

    rgb = "#%02x%02x%02x" % tuple([ int(round((c + m) * 255)) for c in rgb1 ])

    return rgb


