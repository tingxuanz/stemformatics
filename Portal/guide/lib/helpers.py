"""Helper functions

Consists of functions to typically be used within templates, but also
available to Controllers. This module is available to both as 'h'.
"""
# Import helpers as desired, or define your own, ie:
from webhelpers.html import escape, HTML, literal, url_escape
from webhelpers.html.tags import *
from webhelpers import paginate
from routes import url_for
from pylons import url
from guide.lib.recaptcha_client import Recaptcha
recaptcha = Recaptcha()
from guide.lib.stemformatics_helper import print_paginate,setup_accession_ids_for_viewing,get_citations,get_citations_part,setup_email_to_contributing_author,create_letter_for_annotator,web_asset_url
