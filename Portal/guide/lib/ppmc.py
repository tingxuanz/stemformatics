def calcPPMC(a, b):
    """\
An implementation of the Pearson Product Moment Correlation
calculation done on two python lists.   Assumes that a and b
are non-empty lists of numerics and are of equal length.

This is modeled on the formula described in Wikipedia here:
http://changingminds.org/explanations/research/analysis/pearson.htm

    r = SUM((xi - xbar)(y - ybar)) / ((n - 1) * sx * sy)

Where x and y are the variables, xi is a single value of x, 
xbar is the mean of all x's, n is the number of variables, and 
sx is the standard deviation of all x's.

To test:  run "python -m doctest ppmc.py":

>>> p = [1, 3, 5, 6, 8, 9, 6, 4, 3, 2]
>>> q = [2, 5, 6, 6, 7, 7, 5, 3, 1, 1]

>>> print round(calcPPMC(p, q), 4)
0.8547

(Note: this uses the `sample standard deviation` rather than the
standard deviation of the sample.)

"""
    from math import sqrt
    
    # By making N a float, all the other numbers and calculations
    # that use it will be coerced to float.  This obviates the
    # need to iterate through the two incoming lists to ensure
    # their elements are all float.  They need only be numeric.
    N = float(len(a))
    
    # Sanity check: must be non-empty lists
    #  and the length of each must be the same.
    if N < 1 or N != len(b):  return None
    
    # Calc the means
    a_bar = sum(a) / N
    b_bar = sum(b) / N    
    
    # Calc the diffs from the mean (not the variances)
    a_diff = map(lambda x: x - a_bar, a)
    b_diff = map(lambda x: x - b_bar, b)
    
    # Cal the sample std devs
    a_sdev = sqrt(sum(map(lambda x: x**2, a_diff)) / (N-1))
    b_sdev = sqrt(sum(map(lambda x: x**2, b_diff)) / (N-1))
    
    # Return the result.
    return sum(map(lambda x,y: (x * y), a_diff, b_diff)) / ((N - 1) * a_sdev * b_sdev)

