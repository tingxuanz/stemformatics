<%inherit file="/default.html"/>
<%namespace name="Base" file="/base.mako"/>
<%def name="includes()">   
    <script type="text/javascript" src="${h.url('/js/tests.js')}"></script>
</%def>
<style> td:nth-child(2){ text-align: right; } </style>
<%
tests = [
["Graphs", "Large number of samples and probes Novershtern PSG4 ", "/expressions/result?graphType=scatter&datasetID=5003&gene=ENSG00000243137&db_id=56"],
["PG non-microarray", "miRNA  ", "/expressions/feature_result?graphType=default&db_id=46&feature_type=miRNA&feature_id=mmu-mir-205&datasetID=6128"],
["PG non-microarray", "Global Protein ", "/expressions/result?graphType=default&gene=ENSMUSG00000027547&db_id=46&datasetID=6130#"],
["PG non-microarray", "Histone ChIPSeq ", "/expressions/result?graphType=default&datasetID=6151&gene=ENSMUSG00000035206&db_id=46#"],
["PG non-microarray", "DNA Methylation ", "/expressions/result?graphType=default&gene=ENSMUSG00000074637&db_id=46&datasetID=6131"],
["General", "Speed Tests ", "/contents/speed_test"],
["Private datasets", "Logout and then run this ", "/datasets/search?filter=private"],
["Genes", "Partial gene search mincl", "/genes/search?gene=mincl"],
["Genes", "Gene found with search ", "/genes/search?gene=mincl&ensembl_id=ENSG00000166523"],
["Genes", "Adding a Gene list ", "/workbench/gene_set_bulk_import_manager"],
["Genes", "Feature Search - should select a dataset ", "/expressions/feature_result?graphType=default&feature_type=miRNA&feature_id=mmu-mir-148b&db_id=46"],
["Probes", "Multi-mapping probes page ", "/probes/multi_map_summary?probe_id=ILMN_1665632&chip_type=7&db_id=56"],
["Graphs", "Partial gene entered into Gene Expression Graphs ", "/expressions/result?graphType=box&datasetID=5005&gene=POU&db_id=56&sortBy=Sample%20Type"],
["Genes", "Gene Search not found jjjjj ", "/genes/search?gene=jjjjj"],
["Datasets", "Dataset Search not found jjjj ", "/datasets/search?filter=jjjj"],
["Hamlet", "Hamlet small ", "/hamlet/index?dataset=5032"],
["Graphs", "Multi-mapping probes Oct4 -  Gene Expression Graphs - Hough ", "/expressions/result?graphType=default&gene=ENSG00000204531&db_id=56&datasetID=5005"],
["Graphs", "Line Graphs -  Gene Expression Graphs - Hitchens ", "/expressions/result?graphType=default&datasetID=6124&gene=ENSMUSG00000030142&db_id=46"],
["Projects", "Project Grandiose ", "/project_grandiose"],
["Graphs/Genes", "Gene Search ", "/genes/search?gene=POU5F1"],
["Graphs", "Gene Expression Graphs - with example from RNASeq negative numbers ", "/expressions/result?graphType=box&datasetID=6197&gene=ENSMUSG00000030142&db_id=46"],
["Graphs", "Gene Expression Graphs with values less than 1 - Pera Dataset ", "/expressions/probe_result?graphType=default&datasetID=6081&probe=ACVR2B&db_id=56"],
["Graphs", "Gene Expression Graphs ", "/expressions/result?graphType=default&datasetID=5005&gene=ENSG00000229094&db_id=56"],
["Datasets", "Datasets Select Matigian ", "/datasets/search?filter=2000&ds_id=2000"],
["Graphs", "Probe Expression Graphs ", "/expressions/probe_result?graphType=default&datasetID=5005&probe=ILMN_1912512&db_id=56"],
["Graphs", "Feature Expression Graphs ", "/expressions/feature_result?graphType=default&feature_type=miRNA&feature_id=mmu-mir-27b&db_id=46&datasetID=6128"],
["Graphs", "Multi-Gene Expression Graphs ", "/workbench/histogram_wizard?graphType=default&db_id=56&gene_set_id=779&datasetID=5005"],
["Graphs", "Multiview Graphs ", "/expressions/multi_dataset_result?graphType=scatter&gene=ENSG00000229094&db_id=56"],
["Graphs", "Yugene Graphs ", "/genes/summary?gene=ENSG00000229094&db_id=56"],
["Graphs", "Gene Neighbourhood Choose Probe Graphs", "/workbench/gene_neighbour_wizard?gene=ENSG00000115415&db_id=56&datasetID=5005"],
["Graphs", "Select Probes GEG ", "/expressions/result?graphType=default&datasetID=5005&gene=ENSG00000229094&db_id=56&select_probes=ILMN_1679060"],
["Graphs", "Select Probes MGEG ", "/workbench/histogram_wizard?graphType=default&db_id=56&gene_set_id=779&datasetID=5005&select_probes=ILMN_1711899|ILMN_1740938|ILMN_1694877"],
["Graphs", "Rohart MSC Test", "/workbench/rohart_msc_graph?ds_id=6037"],
["Mappings", "Download mapping id #1", "/mappings/mapping_1.txt"]
]


%> 
%if c.debug:

<textarea style="width:1000px;height:500px;">
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head profile="http://selenium-ide.openqa.org/profiles/test-case">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="selenium.base" href="${c.external_base_url}" />
<title>test</title>
</head>
<body>
<table cellpadding="1" cellspacing="1" border="1">
<thead>
<tr><td rowspan="1" colspan="3">test</td></tr>
</thead><tbody>
% for row in tests:
    <%
        type  = row[0]
        test = row[1]
        url = row[2]

    %> 
    <tr>
        <td>open</td>
        <td>${url}</td>
        <td></td>
    </tr>
    <tr>
        <td>pause</td>
        <td>2000</td>
        <td></td>
    </tr>
%endfor


</tbody></table>
</body>
</html>
</textarea>

%else:
<div class="content">    
    <a href="${h.url('/main/tests?debug=true')}">Get code for Selenium</a>
    <div class="baseMarginLeft base_width_minus_margin">
        <button id="open_all_links">Open All</button>
        <table id="tests">
            <thead>
                <tr>
                    <th class="long">Type</th>
                    <th class="long">Test</th>
                    <th class="short">Click</th>
                    
                </tr>
            </thead>
            <tbody>

            % for row in tests:
            <%
                type  = row[0]
                test = row[1]
                url = row[2]

            %> 
                
            <tr>
                <td>${type}</td>
                <td>${test}</td>
                <td><a href="${url}"> Click here </a></td>
            </tr>

            %endfor

            </tbody>
        </table>


    </div>
</div>
                
%endif
