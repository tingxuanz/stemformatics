<%inherit file="/default.html"/>
<%namespace name="Base" file="/base.mako"/>

<%def name="includes()">
    <script type="text/javascript" src="${h.web_asset_url('/js/datasets/search.js')}"></script>
</%def>

<%
        try:
            c.num_of_ds = len(c.dataset)
        except:
            c.num_of_ds = 0
%>

<%def name="show_all_option()">
</%def>
        
<div id="selected_ds_id" class="hidden">${c.selected_ds_id}</div>
<div id="ds_id" class="hidden">${c.ds_id}</div>
<div id="db_id" class="hidden">${c.db_id}</div>
<div id="num_of_datasets" class="hidden">${c.num_of_ds}</div>



<div class="content">

    ${Base.pre_enclosed_search_box()}
    <% 
    text.title = "Search Dataset in "+c.site_name
    text.help = "Keyword Search to filter available datasets: Filter on cell type, author name, publication title or dataset ID"
    text.input_id = "dataset_search"
    text.search_button_id = "view_datasets"
    text.search_action = "/datasets/search"
    text.search_value = c.searchQuery
    text.input_name = "filter"
    text.input_class = ""
    %>
    ${Base.enclosed_search_box(text,self.show_all_option)}



    %if c.num_of_ds == 0:
        <div class="base_width_minus_margin">
            <div class="title gene_search_headers">Quick Search Terms</div>
            <% hover_text = "Click to do an easy search for all the human datasets in "+c.site_name %>
            ${Base.large_icon('analyses','gla_logo','All Human Datasets','Click for all human datasets',hover_text,h.url('/datasets/search?filter=Homo sapiens'))}           
            <% hover_text = "Click to do an easy search for all the Project Grandiose datasets in "+c.site_name %>
            ${Base.large_icon('analyses','gla_logo','All Project Grandiose Datasets','Click for all Project Grandiose datasets',hover_text,h.url('/datasets/search?filter=Project Grandiose:'))}           
            <% hover_text = "Click to do an easy search for all the human msc datasets in "+c.site_name %>
            ${Base.large_icon('analyses no_margin_right','gla_logo','All Human MSC Datasets','Click for all MSC human datasets',hover_text,h.url('/datasets/search?filter=Homo sapiens and MSC'))}           
            <% hover_text = "Click to do an easy search for all the mouse datasets in "+c.site_name %>
            ${Base.large_icon('analyses','gla_logo','All Mouse Datasets','Click for all mouse datasets',hover_text,h.url('/datasets/search?filter=Mus musculus'))}           
            <% hover_text = "This is the Multiple Dataset Downloader. You can search on the metadata of samples and datasets to find datasets that you can then download in the one place."  %>
            ${Base.large_icon('analyses','mdd_logo','Multiple Dataset Downloader','Search and download datasets',hover_text,h.url('/workbench/download_multiple_datasets'))}           
            <% hover_text = "Click to make a suggestion for a dataset in "+c.site_name %>
            ${Base.large_icon('analyses no_margin_right','gla_logo','Suggest a Dataset','Click to suggest a public dataset',hover_text,h.url('/main/suggest_dataset'))}           
        </div>
        <div class="clear"></div>
        <div class="two_cols links_we_like light_background hidden"> 
            <div class="title analyses_text">Human Datasets</div>
            <div class="initial_search"><a href="${h.url('/datasets/search?filter=Homo sapiens')}">Filter All Human Datasets ></a></div>
            <div class="initial_search"><a href="${h.url('/datasets/search?filter=Homo sapiens and Kidney')}">Filter All Human Kidney Datasets ></a></div>
            <div class="initial_search"><a href="${h.url('/datasets/search?filter=Homo sapiens and iPSC')}">Filter All Human iPSC Datasets ></a></div>
            <div class="initial_search"><a href="${h.url('/datasets/search?filter=Homo sapiens and MSC')}">Filter All Human MSC Datasets ></a></div>
        </div>
        <div class="two_cols links_we_like no_margin_right light_background hidden"> 
            <div class="title analyses_text">Mouse Datasets</div>
            <div class="initial_search"><a href="${h.url('/datasets/search?filter=Mus musculus')}">All Mouse Datasets ></a></div>
            <div class="initial_search"><a href="${h.url('/datasets/search?filter=Mus musculus and Kidney')}">All Mouse Kidney Datasets ></a></div>
            <div class="initial_search"><a href="${h.url('/datasets/search?filter=Mus musculus and iPSC')}">All Mouse iPSC Datasets ></a></div>
            <div class="initial_search"><a href="${h.url('/datasets/search?filter=Mus musculus and MSC')}">All Mouse MSC Datasets ></a></div>
        </div>
    %else:
        <% ds_id = c.ds_id %>
        <% 
        chip_type =c.dataset[c.ds_id]['chip_type'] 
        platform_type = c.assay_platform_dict[chip_type]['platform_type'] 
        probe_name = c.assay_platform_dict[chip_type]['probe_name'] 
        %>
        <div class="hidden" id="platform_type">${platform_type}</div>
        <div class="hidden" id="probe_name">${probe_name}</div>
         <div class="dataset_summary_box">
            %if c.role =="admin" or c.role=="annotator":
            <div class="admin">
                <a href="${h.url('/admin/annotate_dataset?ds_id='+str(c.ds_id))}">Annotate this Dataset</a>
                %if c.role=="admin":
                <a href="${h.url('/admin/update_datasets?ds_id='+str(c.ds_id))}">Update this Dataset</a>
                <a href="${h.url('/admin/setup_new_dataset/'+str(c.ds_id))}">Setup this Dataset in Redis</a>
                %endif
            </div>
            %endif 
            <div class="title">${c.dataset[ds_id]['title']}</div>
            <div class="handle">${c.dataset[ds_id]['handle']} (${c.dataset[ds_id]['organism']})</div>
            <div class="cells">${c.dataset[ds_id]['cells_samples_assayed']}</div>
            <div class="clear"></div>
            <div class="description">${c.dataset[ds_id]['authors']}</div>
            <div class="description">${c.dataset[ds_id]['description']}</div>
            <div class="clear"></div>
            <div class="dataset_links_col no_margin_left">
                <table id="datasetDetails"  class="fixed" data-ds_id="${c.ds_id}" >
                    <tr class="pubMedID">
                        <td>Pubmed ID:</td>
                        <td class="detail"><a target="_blank" href="${'http://www.ncbi.nlm.nih.gov/pubmed/' + c.dataset[ds_id]['pub_med_id']}">${c.dataset[ds_id]['pub_med_id']}</a>
                        </td>
                    </tr>
                    <tr class="accessionID">
                        <td>Accession IDs:  </td> 
                        <td class="detail">
                            ${h.setup_accession_ids_for_viewing(c.dataset[ds_id])}
                            %if False == True:
                            %endif 
                        </td>
                    </tr>

%if platform_type == 'miRNA':
    %if c.dataset[ds_id]["top_miRNA"] != {}:
                    <tr class="platform">
                        <td>miRNA of Interest:</td>
                        <td class="genes_of_interest">
                            <% miRNA_of_interest = c.dataset[ds_id]["top_miRNA"].split(',')%>
                            % for row in miRNA_of_interest:
                            
                                    <% 
                                    miRNA = row
                                    main_url =  h.url('/expressions/feature_result?graphType=default&feature_type=miRNA&feature_id=' + str(miRNA) + '&db_id=' + str(c.db_id) + '&datasetID=' + str(c.ds_id)) 
                                    %>
                                    <a href="${main_url}">${miRNA}</a>  
                            % endfor
                        </td>
                    </tr>
    %endif
%else:
    %if c.dataset[ds_id]["top_diff_exp_genes"] != {}:
                    <tr class="platform">
                        <td>Genes of Interest:</td>
                        <td class="genes_of_interest">
                            <% genes_of_interest = c.dataset[ds_id]["top_diff_exp_genes"] %>
                            <% default_url_geg = "" %>
                            % for gene, gene_details in genes_of_interest.iteritems():
                            
                                    <% 
                                    main_url =  h.url('/expressions/result?graphType=default&gene=' + str(gene_details['ensemblID']) + '&db_id=' + str(gene_details['db_id']) + '&datasetID=' + str(c.ds_id)) 
                                    if default_url_geg == "": 
                                        default_url_geg = main_url
                                    %>
                                    <a href="${main_url}">${gene}</a>  
                            % endfor
                        </td>
                    </tr>
    %endif
%endif
                    <tr class="name">
                        <td>Name:</td>
                        <td class="detail">${c.dataset[ds_id]['name']}</td>
                    </tr>
                    <tr class="email">
                        <td>Email:</td>
                        <td class="detail">${c.dataset[ds_id]['email']}</td>
                    </tr>
                    <tr class="affiliation">
                        <td>Affiliation:</td>
                        <td class="detail">${c.dataset[ds_id]['affiliation']}</td>
                    </tr>
                    <tr class="platform">
                        <td>Number of Samples:</td>
                        <td class="detail">${c.dataset[ds_id]['number_of_samples']}</td>
                    </tr>
                    <tr class="platform">
                        <td>Platform:</td>
                        <td class="detail">${c.dataset[ds_id]['platform']}</td>
                    </tr>
%if True == False:
                    <tr class="probes-detected">
                        <td>${Base.print_plural(probe_name)} Detected:</td>
                        <td class="detail">${c.dataset[ds_id]['probes detected']}</td>
                    </tr>
                    <tr class="probes">
                        <td>${Base.print_plural(probe_name)}:</td>
                        <td class="detail">${c.dataset[ds_id]['probes']}</td>
                    </tr>
%endif
                </table>

            </div>
            <div class="clear"></div>
            <div class="dataset_summary_button export_more_info"><a href="#">Export Metadata / View Sample Summary</a></div>
            % if c.msc_values_access in c.dataset[ds_id] and c.dataset[ds_id][c.msc_values_access] == 'True':
            <div class="dataset_summary_button margin_left_small"><a href="${h.url('/workbench/rohart_msc_graph?ds_id=')}${c.ds_id}">Rohart MSC Test</a></div>
            %endif
            <div class="clear"></div>
            
       </div>
            <div class="clear"></div>
         <%def name="show_all_option_null()"> </%def>

        %if platform_type == 'miRNA':
            ${Base.pre_enclosed_search_box()}
            <% 
            text.title = "miRNA Search for Expression Graph"
            text.help = "Enter miRBASE v18 details for more precise results. It will provide suggestions via an autocomplete after four characters."
            text.input_id = "miRNASearch"
            text.search_button_id = "viewmiRNA"
            text.search_action ='#' 
            text.search_value = ''
            text.input_class = 'geg'
            %>
            ${Base.enclosed_search_box(text,self.show_all_option_null)}
        %endif

        ${Base.pre_enclosed_search_box()}
        %if platform_type == 'NGS methylome':
            <% 
            text.title = "Gene Search for Promoter Methylation Graph"
            %>
        %else:
            <% 
            text.title = "Gene Search for Gene Expression Graph"
            %>
        %endif
        <%
        text.help = "Enter Symbol or Ensembl IDs for more precise results. It will provide suggestions via an autocomplete after four characters."
        text.input_id = "geneSearch"
        text.search_button_id = "viewGenes"
        text.search_action ='#' 
        text.search_value = ''
        text.input_class = 'geg'
        %>
        ${Base.enclosed_search_box(text,self.show_all_option_null)}

        %if probe_name != 'miRNA' and probe_name != 'Gene':
            ${Base.pre_enclosed_search_box()}
            <% 
            text.title = probe_name + " Search for Expression Graph"
            text.help = "Enter an ID into the search box below. It will provide suggestions via an autocomplete after two characters."
            text.input_id = "probeSearch"
            text.search_button_id = "viewProbes"
            text.search_action ='#' 
            text.search_value = ''
            text.input_class = 'miRNA'
            %>
            ${Base.enclosed_search_box(text,self.show_all_option_null)}
        %endif




       <div class="clear"></div>



        <div class="more_info">
            <div class="title">Export / View Sample Summary</div>
            <div class="export">
               <ul class="buttonMenus">
                    <li id="exportMenu">
                        <a class="button dropdown">
                            <span><span class="icon go"></span>Export</span><span class="arrow down"></span>
                        </a>
                        <ul class="submenu">
                            %if c.dataset_status != 'Limited':

                            <li><a href="${h.url('/datasets/download_gct/' + str(c.ds_id))}">Download GCT expression file</a></li>
                            %if 'show_yugene' in c.dataset[ds_id] and c.dataset[ds_id]['show_yugene'] == True:
                            <li><a href="${h.url('/datasets/download_yugene/' + str(c.ds_id))}">Download Yugene expression file</a></li>
                            %endif
                            <li><a href="${h.url('/datasets/download_cls/' + str(c.ds_id))}">Download CLS sample annotation file</a></li>
                            %endif
                            <li><a href="${request.url}&export=true" id="exportTableCSVButton">Export Metadata</a></li>
                            <li><a href="#" id="exportSampleSummaryButton">Export Sample Summary</a></li>
                        </ul>
                    </li>
                </ul>
         
            </div>            
            <div class="clear"></div>
            <div class="breakdown">
                <table id="dataset_sample_summary" class="fixed">
                    <thead>
                        <tr>
                            <th>Sample Grouping</th>
                            <th>Sample Count</th>
                        </tr>
                    </thead>
                    <tbody>
                        % for grouping, count in c.dataset[ds_id]["breakDown"].iteritems():
                            % if grouping.split(': ')[1] != 'NULL':
                                <tr>
                                    <td>${grouping}</td>
                                    <td>${count}</td>
                                </tr>
                            % endif
                        % endfor
                    </tbody>
                </table>
            </div>

        </div>



    %endif    

</div>
