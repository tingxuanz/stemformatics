<%inherit file="/default.html"/>\
<%namespace name="Base" file="../base.mako"/>
<%def name="includes()">

    <script type="text/javascript" src="${h.url('/js/expressions/multi_dataset_result.js')}"  ></script>
    
    % if c.production != 'true' or c.debug is not None:
        <script type="text/javascript" src="${h.url('/js/expressions/graph.js')}"  ></script>
        <script type="text/javascript" src="${h.url('/js/expressions/gene_expression_graph_triggers.js')}"  ></script>
        <script type="text/javascript" src="${h.url('/js/expressions/gene_expression_graphs.js')}"  ></script>
    % else:
        <script type="text/javascript" src="${h.url('/js/expressions/geg.min.js')}"  ></script>
    % endif
 
    
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/flot/0.8.2/jquery.flot.min.js"  ></script>
    <script src="//cdn.jsdelivr.net/kineticjs/4.5.4/kinetic.min.js"  ></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/canvas2image/0.1/base64.js"  ></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/canvas2image/0.1/canvas2image.js"  ></script>

    <script type="text/javascript" src="${h.url('/js/flot/s4m_graphing.min.js')}"  ></script>
    <script type="text/javascript" src="${h.url('/js/md5-min.js')}"  ></script>



    
    <link type="text/css" href="${h.url('/css/genes/gene_info.css')}" rel="stylesheet" />
    <link type="text/css" href="${h.url('/css/expressions/multi_dataset_result.css')}" rel="stylesheet" />
    
    
</%def>

    
    <div id="mult_dataset_graph_header">
        
        <a id="multiChooseDatasets" class="chooseDatasetsButton" href="${c.base_url}&amp;force_choose=yes" ><span>CHANGE DATASETS</span><img src="${h.url('/images/show_genes.png')}"/></a>
        
       ${Base.pre_enclosed_search_box()}
       <%
            text.search_action ='#' 
            text.search_value = c.symbol 
            text.input_id = 'geneSearch'
            text.input_class = 'mv'
       %>

        ${Base.search_box(text)}

    </div>
        
    <div id=overallGraphDiv>         
        <div id=sortBy class="hidden">Sample Type</div>
        <div id="kinetic_legend" class="movable_legend"></div>
        <div class='graphControls'>
            <div id=graphingOptions>
                <div class=hidden>
                   <div id="multi_view_datasets" class="hidden">${c.multi_view_datasets}</div>
                </div>
           </div>
            
           <div class="height12px"></div>
               <div class=innerDiv><div class="title">Multiview Expression Graph for ${c.symbol} </div></div> 
        </div>
        
        
        <div class=clear></div>
        
        
         <% count = 0 %>
        
        % for str_ds_id in c.json_view_data: 
            <% ds_id = int(str_ds_id) %>
            %if count == 0:
                <div class="content first_graph" id="content${ds_id}">
            %else:
                <div class="content" id="content${ds_id}">
            %endif 
            <% count+= 1 %>
                <div id="json_view_data${ds_id}"} class="hidden">${c.json_view_data[ds_id]}</div>
                
                <div id=ensemblID${ds_id} class="hidden">${c.ensemblID}</div>
                <div id=datasetID${ds_id} class="hidden">${ds_id}</div>
                
                
                <div class='backgroundGraph'>
                    <div class="innerDiv">
                        <div class="innerBackgroundGraph">
                            <div class="innerDiv" id="white_background_control">
        


                                <ul class="buttonMenus">
                                    <li id="exportMenu">
                                        <a class="button dropdown"><span><span class="icon go"></span>Export &amp; Share</span><span class="arrow down"></span></a>
                                        <ul class="submenu">
                                            <li><a href="#" class="export_graph_data" data-id="${ds_id}">Export Graph Data</a></li>
                                            <li><a href="#" data-id="#graph${ds_id}" class="exportImageButton">Export Graph As Image</a></li>
                                            <li><a href="#" data-id="#kinetic_legend" class="exportLegendButton">Export Legend As Image</a></li>
                                            <li><a href="#" id="share_link" class="share_link">Share</a></li> 
                                        </ul>
                                    </li>
                                     <li id="graphOptionsMenu">
                                        <a class="button dropdown"><span><span class="icon eye"></span>Graph Options</span><span class="arrow down"></span></a>
                                        <ul class="submenu">
                                            <li><a href="#" class=chooseGraphType clickChoose=box >Choose Boxplot Graph</a></li>
                                            <li><a href="#" class=chooseGraphType clickChoose=bar >Choose Bar Graph</a></li>
                                            <li><a href="#" class=chooseGraphType clickChoose=scatter >Choose Scatter Graph</a></li>
                                            <li><a href="#" data-ds_id=${ds_id} class="no_set_min_y_axis_link" >Toggle Automatic Minimum Y axis</a></li>
                                            <li class=chooseSD ><a href="#" class="toggle_sd" data-ds_id=${ds_id} id=toggleSD >Toggle Standard Deviation</a></li>
                                            <li><a href="#" ds_id="${ds_id}" class="show_legend" >Show Legend</a></li>

                                        </ul>
                                    </li>
                                    <li id="analysisMenu">
                                        <a class="button dropdown"><span><span class="icon glass"></span>Analysis</span><span class="arrow down"></span></a>
                                        <ul class="submenu">
                                            <li><a id="YugeneButton" href="${h.url('/genes/summary?gene='+ str(c.view_data[ds_id].ensembl_id)+'&db_id='+str(c.db_id))}" >Yugene Interactive Graph: all datasets</a></li>
                                        % if c.uid and c.view_data[ds_id].ref_type == 'ensemblID' and c.dataset_status == "Available":
                                                % if c.allow_genePattern_analysis:
                                                    <li><a id="geneNeighbourhoodButton" href="${h.url('/workbench/gene_neighbour_wizard?datasetID='+ str(c.view_data.ds_id) + '&gene='+ str(c.view_data.ensembl_id))}" >Gene Neighbourhood: find similar expression profiles</a></li>
                                              % endif
                                                <li><a id="foldChangeViewerButton"  href="${h.url('/workbench/fold_change_viewer_wizard?datasetID='+ str(c.view_data.ds_id) + '&gene='+ str(c.view_data.ensembl_id))}" >Fold Change Viewer</a></li>
                                   
                                        % endif
                                        </ul>
                                % if c.view_data[ds_id].ref_type == 'ensemblID':
                                    ${Base.genomeMenu()}  
                                %endif
                                % if c.view_data[ds_id].ref_type == 'ensemblID':
                                    <li id="infoMenu">
                                        <a class="geneInfoButton button"><span><span class="icon info"></span>Information</span><span class="arrow right"></span></a>
                                    </li>
                                % endif
                                    <li id="helpMenu">
                                        <a href="#" class="button helpPopup"><span><span class="icon quest"></span>Help</span><span class="arrow right"></span></a>
                                    </li>
                                </ul>

                                <div class='displayGraphs loading'>
                                
                                    <div id=newLegend${ds_id}></div>
                                    
                                    <div class="innerDiv">
                                        <div id="xaxis_labels${ds_id}" class="xaxis_labels" ></div>
                                        <div id="graph${ds_id}" class="graph graph_${c.view_data[ds_id].graph_type}">
                                        </div>
                                        <div class=clear></div>
                                    </div>
                                    <div class=clear></div>
                                </div>
                                <div class=clear></div>
                            </div>
                        </div>
                    </div>
                    <div class=clear></div>
                </div>
                <div class=clear></div>
                <a name="showData${ds_id}"></a>
                <div id='show_graph_data${ds_id}' class='view_table_content hidden'>
                    ${Base.display_gene_expression_graph_values(c.view_data[ds_id])}
  
                </div>
                <div id='showRawTableData${ds_id}'>
                    
                </div>
                    
            </div>
        
           
        % endfor

        ${Base.displayGeneDetail()}  

        </div> 
      
    <!-- </div> -->
