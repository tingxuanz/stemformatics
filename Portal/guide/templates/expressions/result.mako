<%inherit file="/default.html"/>\
<%namespace name="Base" file="../base.mako"/>
<%def name="includes()">
    <script type="text/javascript" src="${h.url('/js/expressions/result.js')}"  ></script>
    
    % if c.production != 'true' or c.debug is not None:
        <script type="text/javascript" src="${h.url('/js/expressions/gene_expression_graph_triggers.js')}"  ></script>
        <script type="text/javascript" src="${h.url('/js/expressions/gene_expression_graphs.js')}"  ></script>
        <script type="text/javascript" src="${h.url('/js/expressions/graph.js')}"  ></script>
    % else:
        <script type="text/javascript" src="${h.url('/js/expressions/geg.min.js')}"  ></script>
    % endif
 
    
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/flot/0.8.2/jquery.flot.min.js"  ></script>
    <script src="//cdn.jsdelivr.net/kineticjs/4.5.4/kinetic.min.js"  ></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/canvas2image/0.1/base64.js"  ></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/canvas2image/0.1/canvas2image.js"  ></script>
    <script type="text/javascript" src="${h.url('/js/flot/s4m_graphing.min.js')}"  ></script>

    <script type="text/javascript" src="${h.url('/js/md5-min.js')}"  ></script>
    
    <link type="text/css" href="${h.url('/css/genes/search.css')}" rel="stylesheet" />
    <link type="text/css" href="${h.url('/css/expressions/result.css')}" rel="stylesheet" />
    <link type="text/css" href="${h.url('/css/genes/gene_info.css')}" rel="stylesheet" />
    
</%def>

<% 
probe_name = c.assay_platform_dict[c.view_data.chip_type]['probe_name']
%>

<div class="hidden" id="probe_name">${probe_name}</div>

<div id="text_overlay">
</div>
    <div class="content">
        <div id="db_id" class="hidden">${c.db_id}</div>
        <div id="choose_dataset_immediately" class="hidden">${c.choose_dataset_immediately}</div>
        <div id="ds_id" class="hidden">${c.ds_id}</div>
        <div id="json_view_data${c.ds_id}" class="hidden">${c.json_view_data}</div>
        % if c.view_data.ref_type == 'gene_set_id':
        <div id="gene_set_id" class="hidden">${c.view_data.ref_id}</div>
        %endif
 
        % if c.view_data.ref_type == 'ensemblID':
            <div class="breadcrumbs"><a class="basic_link" href="${h.url('/genes/search')}">Gene Search</a> >> <a class="basic_link" id="searchGene" href="${h.url('/genes/search?gene=')}${c.view_data.symbol}">${c.view_data.symbol}</a> >> <a class="basic_link" href="${h.url('/genes/summary?gene=')}${c.view_data.ensembl_id}&db_id=${c.db_id}">${c.view_data.symbol} Summary</a> >> <a class="basic_link" href="${h.url('/datasets/search')}">Dataset Browser </a>>> <a class="basic_link" href="${h.url('/datasets/summary?datasetID=')}${c.view_data.ds_id}">${c.view_data.handle} Summary</a> >> <span class="current">Expression Results</span></div>
        %else:
            ${Base.wb_breadcrumbs()}  
       
        %endif
        <div class="clear">
        <div class='backgroundGraph'>
            <div class="innerDiv">
 
                <div class="innerBackgroundGraph">
                    <div class="innerDiv" id="white_background_control">
                        <div class='graphControls'>





          <a id="chooseDatasets" class="chooseDatasetsButton" href="${request.url.replace('result?','choose_dataset?')}" ><span>CHANGE DATASETS</span><img src="${h.url('/images/show_genes.png')}"/></a>
            % if c.view_data.ref_type == 'gene_set_id':
                  <a id="chooseGeneList" class="chooseDatasetsButton" href="${h.url('/workbench/histogram_wizard?datasetID='+str(c.ds_id))}" ><span>CHANGE GENE LIST</span><img src="${h.url('/images/show_genes.png')}"/></a>
                     
            %endif
           <div id=datasetSelectDiv> </div>
           ${Base.pre_enclosed_search_box()}
           %if c.view_data.ref_type == 'ensemblID':
               <%
                    text.search_action ='#' 
                    text.input_id = 'geneSearch'
                    text.search_value = c.symbol 
                    text.input_class = 'geg'
               %>
             ${Base.search_box(text)}
           %endif
           %if c.view_data.ref_type == 'probeID':
               <%
                    text.search_action ='#' 
                    text.input_id = 'probeSearch'
                    text.search_value = c.ref_id 
                    text.input_class = 'geg'
               %>
             ${Base.search_box(text)}
           %endif
           %if c.view_data.ref_type == 'miRNA':
               <%
                    text.search_action ='#' 
                    text.input_id = 'featureSearch'
                    text.search_value = c.ref_id 
                    text.input_class = 'geg'
               %>
             ${Base.search_box(text)}
          %endif

                            <div class="clear"></div>
                                <ul class="buttonMenus">
                                    <li id="exportMenu">
                                        <a class="button dropdown"><span><span class="icon go"></span>Export &amp; Share</span><span class="arrow down"></span></a>
                                        <ul class="submenu">
                                            <li><a href="#" data-id="${c.ds_id}" class="export_graph_data">Export Graph Data</a></li>
                                            <li><a href="#" data-id="#graph" class="exportImageButton">Export Graph As Image</a></li>
                                            <li><a href="#" data-id="#kinetic_legend" class="exportLegendButton">Export Legend As Image</a></li>
                                            <li><a href="#" id="share_link">Share</a></li> 
                                        </ul>
                                    </li>
                                     <li id="graphOptionsMenu">
                                        <a class="button dropdown"><span><span class="icon eye"></span>Graph Options</span><span class="arrow down"></span></a>
                                        <ul class="submenu">
                                            % if c.view_data.limit_sort_by != 'Sample Type':
                                                <% tempList = c.view_data.limit_sort_by.split(',') %> 
                                                % for sort_by in tempList: 
                                                    <li><a href="#" class=chooseSortType clickChoose="${sort_by}">Group By ${sort_by}</a></li>
                                                %endfor 
                                            % endif
                                            <li><a href="#" class=chooseGraphType clickChoose=box >Choose Boxplot Graph</a></li>
                                            <li><a href="#" class=chooseGraphType clickChoose=bar >Choose Bar Graph</a></li>
                                            <li><a href="#" class=chooseGraphType clickChoose=scatter >Choose Scatter Graph</a></li>
                                            % if c.view_data.line_graph_available: 
                                                <li><a href="#" class=chooseGraphType clickChoose=line >Choose Line Graph</a></li>
                                            % endif
                                            <li><a href="#" id=zoomHorizontalGraphButton >Toggle Expand Graph Horizontally</a></li>
                                            <li><a href="#" id="no_set_min_y_axis_link" >Toggle Automatic Minimum Y axis</a></li>
                                            <li class=chooseSD ><a href="#" id=toggleSD >Toggle Standard Deviation</a></li>
                                            <li class="toggle_select_probes" ><a href="#" id="toggle_select_probes" >Toggle Select ${probe_name}</a></li>
                                        </ul>
                                    </li>
                                    <li id="analysisMenu">
                                        <a class="button dropdown"><span><span class="icon glass"></span>Analysis</span><span class="arrow down"></span></a>
                                        <ul class="submenu">
                                            <li><a href="${h.url('/datasets/search?ds_id='+str(c.view_data.ds_id))}" >Dataset Summary Information</a></li>
                                        % if c.view_data.ref_type != 'gene_set_id':
                                            % if c.view_data.ref_type == 'ensemblID':
                                            <li><a id="YugeneButton" href="${h.url('/genes/summary?gene='+ str(c.view_data.ensembl_id)+'&db_id='+str(c.db_id))}" >Yugene Interactive Graph: all datasets</a></li>
                                            <li><a target="_blank" href="${c.innate_db_object.get_single_gene_url(c.view_data.ensembl_id)}" >Innate DB Gene Information</a></li>
                                            <li><a target="_blank" href="${c.string_db_object.get_single_gene_url(c.view_data.ensembl_id)}" >String DB Gene Information</a></li>
                                            %endif
                                        %else:
                                            <li><a href="${h.url('/workbench/gene_set_view/'+str(c.view_data.ref_id))}" >Gene List Information</a></li>
                                            <li><a id="hc" href="${h.url('/workbench/hierarchical_cluster_wizard?gene_set_id='+str(c.view_data.ref_id)+'&datasetID='+str(c.view_data.ds_id))}" >Hierarchical Cluster using the Gene List</a></li>
                                            %if c.select_probes is not None:
                                            <li><a id="hc" href="${h.url('/workbench/hierarchical_cluster_wizard?select_probes='+str(c.select_probes)+'&datasetID='+str(c.view_data.ds_id))}" >Hierarchical Cluster using selected probes</a></li>
                                            %endif
                                            <li><a id="gla" href="${h.url('/workbench/gene_set_annotation_wizard?gene_set_id='+str(c.view_data.ref_id))}" >Gene List Annotation</a></li>
    
                                        %endif
                                        % if c.uid and c.view_data.ref_type == 'ensemblID':
                                            <li><a id="multiviewButton" href="${h.url('/expressions/multi_dataset_result?graphType=scatter&gene='+ str(c.view_data.ensembl_id)+'&db_id='+str(c.db_id))}" >Multiview Expression Graph: view 4 datasets</a></li>
                                            
                                        % endif
                                        % if c.uid and c.view_data.ref_type == 'ensemblID' and c.dataset_status == "Available":
                                                % if c.allow_genePattern_analysis:
                                                    <li><a id="geneNeighbourhoodButton" href="${h.url('/workbench/gene_neighbour_wizard?datasetID='+ str(c.view_data.ds_id) + '&gene='+ str(c.view_data.ensembl_id)+'&db_id='+str(c.db_id))}" >Gene Neighbourhood: find similar expression profiles</a></li>
                                              % endif
                                                <li><a id="foldChangeViewerButton"  href="${h.url('/workbench/fold_change_viewer_wizard?datasetID='+ str(c.view_data.ds_id) + '&gene='+ str(c.view_data.ensembl_id)+'&db_id='+str(c.db_id))}" >Fold Change Viewer</a></li>
                                   
                                        % endif
                                        </ul>
                                </li>
                                % if c.view_data.ref_type == 'ensemblID':
                                    ${Base.genomeMenu()}  
                                %endif
                                % if c.view_data.ref_type == 'ensemblID':
                                    <li id="infoMenu">
                                        <a class="geneInfoButton button"><span><span class="icon info"></span>Information</span><span class="arrow right"></span></a>
                                    </li>
                                % endif
                                    <li id="helpMenu">
                                        <a href="#" class="button helpPopup"><span><span class="icon quest"></span>Help</span><span class="arrow right"></span></a>
                                    </li>
                                </ul>




                        </div>




                        <div class=clear></div>
                      </div>
                <div class=clear></div>
            </div>
            <div class=clear></div>
        </div>
        <div class='displayGraphs loading'>
            <div class="innerDiv">
                <div id="xaxis_labels${c.ds_id}" class="xaxis_labels"></div>
                <div id="graph" class="graph graph_${c.view_data.graph_type}">
                </div>
                <div id="kinetic_legend" class="movable_legend"></div>
            </div>
            <div class=clear></div>
        </div>
        <div class=clear></div>

        <div class=clear></div>
        <a name="showData${c.view_data.ds_id}"></a>
    </div>
    <div class="multi_select">
        <div class="title">Select dataset</div>
        <div class="basic_help">Select a dataset to view</div>
         <% 

            url_link = "&datasetID=" if c.url.find('?') != -1 else "?datasetID="
            
            # strip out any datasetID values in the url
            import re
            expression='\&datasetID=[0-9]+'
            clean_url = re.sub(expression,'',c.url)
            
            expression='\?datasetID=[0-9]+\&'
            clean_url = re.sub(expression,'?',clean_url)
            url = clean_url+url_link
        %>

        ${Base.choose_datasets_table(c.datasets,url)}
    </div>

    <div id='show_graph_data${c.view_data.ds_id}' class='view_table_content'>
        ${Base.display_gene_expression_graph_values(c.view_data)}
    </div>
    
    <div id="export_graph_modal" class="modal">
        <div class="wb_modal_title">
            Export Graph as High Resolution Image
        </div>
        <div class="wb_modal_content">
            <div>
                <select id="exp_size">
                    <option value="a4" selected="selected">3508px x 2480px (A4 @ 300dpi)</option>
                    <option value="a5">2480px x 1748px (A5 @ 300dpi)</option>
                </select>
                <a id="save_button" class="button plain simplemodal-close">Export</a>
            </div>
        </div>
    </div>
    
    <div id="export-wrapper"><div id="export-graph"></div></div>
        % if c.view_data.ref_type == 'ensemblID':
            ${Base.displayGeneDetail()}  
        %endif 

<%def name="show_all_option_null()">

</%def>
    <div id="no_data" class="modal">
        <div class="wb_modal_title">
            No data found for these parameters
        </div>
        <div class="wb_modal_content">
            <div>

                <div class="description margin_bottom_large">
                    <p>No data could be found for this dataset using these parameters.</p>

                    <p>If this is a miRNA dataset, you can search using the <a class="underline" href="${h.url('/genes/feature_search')}">Feature Search</a></p>
                    <p>If you know the underlying identifiers for this dataset, you can select individual <a class="underline" href="${h.url('/expressions/probe_expression_graph?ds_id='+str(c.ds_id))}">${probe_name}.</a></p>

            %if c.role =="admin":
            <div class="admin">
                <a href="${h.url('/admin/setup_new_dataset/'+str(c.ds_id))}">Setup this Dataset in Redis</a>
            </div>
            %endif 



                </div>


                <div class="clear"></div>
            </div>
        </div>
    </div>
  
</div>
