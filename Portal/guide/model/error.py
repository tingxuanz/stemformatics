from guide.model.stemformatics.stemformatics_notification import Stemformatics_Notification # wouldn't work otherwise??
import sys, os,socket

class GeneralS4MError(Exception):
  
    def __init__ (self,e,ds_id=None):
        self.e = e
        self.error_name = e.__class__.__name__
        exc_type, exc_obj, exc_tb = sys.exc_info()
        self.fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        self.line_no = exc_tb.tb_lineno 
        self.message = str(exc_obj.message)
        self.args = exc_obj.args 

        hostname = socket.gethostname() 
        if ds_id is None:
            ds_id = "Not specified"
        
        subject =self.error_name + " ds_id: " + str(ds_id)+ " message:"+self.message + " server:" + hostname
        body =self.error_name + " in program "+self.fname+" on line# "+ str(self.line_no)+" for ds_id: "+str(ds_id)+" : " + self.message 
        Stemformatics_Notification.send_error_email(subject,body) 

