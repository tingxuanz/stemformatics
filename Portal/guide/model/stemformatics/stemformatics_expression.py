import re,string,json,numpy,os,redis,logging
log = logging.getLogger(__name__)
import sqlalchemy as SA
from sqlalchemy import or_, and_, desc

from guide.lib.state import *
from guide.model.stemformatics import *
import psycopg2,psycopg2.extras,cPickle

# Task #500 - wouldn't work otherwise
from stemformatics_probe import *

# Task#465 x PLATFORM
#CRITICAL-6
from guide.model.stemformatics.stemformatics_gene import Stemformatics_Gene # wouldn't work otherwise??
from guide.model.stemformatics.stemformatics_gene_set import Stemformatics_Gene_Set # wouldn't work otherwise??
from guide.model.stemformatics.stemformatics_dataset import Stemformatics_Dataset # wouldn't work otherwise??
from guide.model.stemformatics.stemformatics_notification import Stemformatics_Notification # wouldn't work otherwise??
from guide.model.error import GeneralS4MError

__all__ = ['Stemformatics_Expression']

import formencode.validators as fe

SUBSCRIBER_NAME = fe.Regex("[\w ]*", not_empty=False, if_empty="Anonymous User")
SUBSCRIBER_STATE = fe.Regex("[\w ]*", not_empty=False, if_empty="PENDING")
DESCRIPTIVE_TEXT = fe.Regex("[\w ]*", not_empty=False, if_empty="")
POS_INT = fe.Int(min=1, not_empty=True)
NUMBER = fe.Number(not_empty=True)
IDENTIFIER = fe.PlainText(not_empty=True)
URL = fe.URL(not_empty=True)

class tempData(object):
    pass

def is_multiple(num,divisor):
    """Return whether the number num is even."""
    return num % divisor == 0

class Stemformatics_Expression(object):
    """\
    Stemformatics_Expression Model Object
    ========================================

    A simple model of static functions to make the controllers thinner for expression result and mini graphs in gene search

    Please note for most of these functions you will have to pass in the db object

    All functions have a try that will return None if errors are found

    """    

    def __init__ (self):
        pass
    

        
    @staticmethod
    def get_all_samples_from_dataset(db,ds_id): #CRITICAL-2
        db.schema = 'public'
        bm = db.biosamples_metadata 
        metaDataValues = bm.filter(and_(bm.ds_id==ds_id,bm.md_name=='Replicate Group ID')).all()
        
        sample_list = [  ]
        for item in metaDataValues:
            new_dict = {}
            for this_item in item.__dict__:
                if '_sa_instance_state' not in this_item:
                    new_dict[this_item] = item.__dict__[this_item]
                
            sample_list.append(new_dict)
                
        
        
        
        return sample_list
    
       
       
    def get_mapping_choose_to_view_by_x_platform():
        mapping_dict = {2:"View by Dataset",4:"View by Cell Type",5:"View by Tissue",7:"View by Sample Type"}
        return mapping_dict



    """
    This method is to 
    - clean up the cumulative value (it can be 0)
    - sample the data based on the value of the YuGene data

    split it up into two lists
    - one containing the sampled data
    - one containing the full data

    store both these lists in Redis
    - the sampled data is for other users to be able to re-use this more quickly and
    - the full data is for filtering and being able to provide breakdowns of the data quickly
    """
    @staticmethod
    def _sampling_of_yugene_data(sort_values):
        full_data = []         
        sample_data = []         
        graph_values = {}
        # sampling to limit the total values to 2000 or less
        if len(sort_values) > 1000:
            divisor = int(len(sort_values)/1000)
        else:
            divisor = 1

        count_split = 0 
        for row in sort_values:

            cumulative_value = row[0]

            # YuGene cannot be over 1. If that is the case, 
            # this is bad and this value should be ditched.
            if cumulative_value > 1:
                continue

            if cumulative_value < 0:
                cumulative_value = 0 
   

            temp_list = [cumulative_value] + row[1:]
            full_data.append(temp_list)

            count_split = count_split + 1
            if not is_multiple(count_split,divisor):
                continue 


            # this is the minimum data we need to draw an outline of the graph
            sample_data.append(temp_list)

        
        graph_values['full_data'] = full_data
        graph_values['sample_data'] = sample_data
        return graph_values



    """
    Merging filtered and sample data to make it easy to display
    """
    @staticmethod
    def calculate_yugene_data_for_display(sample_values,filtered_result,merge=False):
        if merge == True:
            temp_data = sample_values + filtered_result
        else:
            temp_data = sample_values
        
        final_data = []

        temp_data.sort(reverse=True)
        x_position = 0
        for row in temp_data:
            if row not in final_data:
                x_position += 1
                row = row + [x_position]
                final_data.append(row)
        return final_data

    """
    This converts the sample_values into a TSV or CSV format.
    It is based on the line in _sampling_of_yugene_data:
        temp_list = [cumulative_value,probe_id,ds_id,chip_type,x_position] 

    """
    @staticmethod
    def convert_yugene_data_to_tsv_csv(sample_values,format_type):
        delimiter = None
        list_of_headers = ['yugene_value','probe_id','sample_id','ds_id','chip_type','x_position'] 
        if format_type == 'tsv':
            delimiter = "\t"

        if format_type == 'csv':
            delimiter = ","

        if delimiter == None:
            return ""

        
        text = delimiter.join(list_of_headers)+"\n"
        for row in sample_values:
            row = map(str, row)
            text += delimiter.join(row)+"\n"
        return text 
 
    """
    This converts the breakdown data into TSV or CSV

    """
    @staticmethod
    def convert_yugene_breakdown_data_to_tsv_csv(values,format_type):
        delimiter = None
        list_of_headers = ['type','name','number']
        if format_type == 'tsv':
            delimiter = "\t"

        if format_type == 'csv':
            delimiter = ","

        if delimiter == None:
            return ""

        
        text = delimiter.join(list_of_headers)+"\n"
        for breakdown_type in values:

            for name in values[breakdown_type]: 
                number = values[breakdown_type][name]
                row = [breakdown_type,name,number]
                row = map(unicode, row)
                text += delimiter.join(row)+"\n"
        return text 
 



    """
    This will set the redis key for the uid and the gene 
    (as different users may have different access to datasets)

    eg. key would be 3|ENSMUSG00000057666|sample_data
 
    it can then be saved later on
    have to store it as json

    Both full_data and sample_data will be saved.
    full_data is for using for filtering based on the values
    sample_data is for showing the general line graph
    """
    @staticmethod
    def set_yugene_graph_values(uid,ensembl_id,db_id,graph_values):
        r_server = redis.Redis(unix_socket_path=config['redis_server'])
        delimiter = config['redis_delimiter']

        expiry_time = 86400 # 24 hours 

 
        label_name = 'full_data'+delimiter+str(uid)+delimiter+ensembl_id+delimiter+str(db_id)
        data = Stemformatics_Expression.store_yugene_graph_values(graph_values['full_data'])
        result = r_server.set(label_name,data)

        if result == True: 
            # expire previous key if it worked
            result = r_server.expire(label_name,expiry_time)
            
            label_name = 'sample_data'+delimiter+str(uid)+delimiter+ensembl_id+delimiter+str(db_id)
            data = Stemformatics_Expression.store_yugene_graph_values(graph_values['sample_data'])
            result = r_server.set(label_name,data)
            if result == True:
                # expire previous key if it worked
                r_server.expire(label_name,expiry_time)
    
        return result 

    """
    Separate out the final data format from the method
    """
    @staticmethod
    def store_yugene_graph_values(data):
        store_data = cPickle.dumps(data)
        return store_data

    """
    This will get the redis key for the uid and the gene 
    (as different users may have different access to datasets)

    eg. key would be 3|ENSMUSG00000057666|full_data
 
    full_data is for using for filtering based on the values
    have to store it as json

    returns NONE if not available
    """
    @staticmethod
    def get_yugene_full_data_graph_values(uid,ensembl_id,db_id):
        r_server = redis.Redis(unix_socket_path=config['redis_server'])
        delimiter = config['redis_delimiter']

        label_name = 'full_data'+delimiter+str(uid)+delimiter+ensembl_id+delimiter+str(db_id)
        result = r_server.get(label_name)
        if result is not None:
            result = Stemformatics_Expression.restore_yugene_graph_values(result)
        return result 
        
    """
    Separate out the final data format from the method
    """
    @staticmethod
    def restore_yugene_graph_values(data):
        restore_data = cPickle.loads(data)
        return restore_data


    """
    This will get the redis key for the uid and the gene 
    (as different users may have different access to datasets)

    eg. key would be 3|ENSMUSG00000057666|sample_data
 
    sample_data is for using for drawing the approximate graph
    have to store it as json
    returns NONE if not available
    """
    @staticmethod
    def get_yugene_sample_data_graph_values(uid,ensembl_id,db_id):
        r_server = redis.Redis(unix_socket_path=config['redis_server'])
        delimiter = config['redis_delimiter']

        label_name = 'sample_data'+delimiter+str(uid)+delimiter+ensembl_id+delimiter+str(db_id)
        result = r_server.get(label_name)
        if result is not None:
            result = Stemformatics_Expression.restore_yugene_graph_values(result)
        return result 
        



    """
    Main function to get the expression data back for Yugene
    x_platform is the old name for YuGene

    Will also save this into redis for later use and expire within 3 hours.
    """
    @staticmethod
    def return_x_platform_matricks_data(db_id,datasets_dict,ensemblID,all_sample_metadata):
        
        #metadata_list = Stemformatics_Expression.get_all_metadata_list_for_x_platform()

        result = Stemformatics_Gene.get_probe_mappings_for_datasets(db_id,datasets_dict,ensemblID) 

        chip_type_mapping_id_dict = result['chip_type_mapping_id_dict']
        mapping_id_probe_list = result['mapping_id_probe_list']

        sort_values = []
        list_view_by_value = []
        
        for ds_id in datasets_dict:
            chip_type = datasets_dict[ds_id]['chip_type']
            handle = datasets_dict[ds_id]['handle']

            try:
                mapping_id = chip_type_mapping_id_dict[chip_type]
                probe_list = mapping_id_probe_list[mapping_id]
            except:
                probe_list = [] 

            if probe_list == []:
                continue

            cumulative_rows = Stemformatics_Expression.get_cumulative_rows(ds_id,probe_list)
            sample_labels = Stemformatics_Expression.get_cumulative_sample_labels(ds_id)
            sort_values =  Stemformatics_Expression._process_yugene_rows(cumulative_rows,sample_labels,ds_id,chip_type,sort_values)

        graph_values = Stemformatics_Expression._sampling_of_yugene_data(sort_values)
        return graph_values


    """
    This gets the absolute minimum we need to draw a line and then find out the metadata we need later on.
    it returns a list of lists so that we can sort on the value easily

    cumulative_rows is a list of values for all the probes for a dataset
    cumulative_sample_labels is a list of all the samples in order
    """
    @staticmethod
    def _process_yugene_rows(cumulative_rows,sample_labels,ds_id,chip_type,sort_values):
        for row in cumulative_rows:
            sample_count = 0
            for expression_value in cumulative_rows[row]:
                probe_id = row
                if expression_value != '' and expression_value != 'None':
                    expression_value = float(expression_value) 
                else:
                    expression_value = -999999999
                chip_id  = sample_labels[sample_count]
                sample_count += 1
                value = expression_value 
              
                row_values_list = [value,probe_id,chip_id,ds_id,chip_type]
                        
                
                # store all the row values including the metadata in sort_values
                sort_values.append(row_values_list)

        return sort_values



    
    @staticmethod
    def return_stepped_colours(list_of_start_end_colours,number_of_steps):
        if list_of_start_end_colours is None:
            list_of_start_end_colours = [['#FFFFFF','#000000']]
        #add in extra step to stop it from going so close to black
        number_of_steps = number_of_steps +1 
        for colour_range in list_of_start_end_colours:
            start_colour = colour_range[0]
            end_colour = colour_range[1]
            
            colour = []
            
            # split the colours into characters
            colour.append([int(start_colour[1:3],16),int(end_colour[1:3],16)])
            colour.append([int(start_colour[3:5],16),int(end_colour[3:5],16)])
            colour.append([int(start_colour[5:7],16),int(end_colour[5:7],16)])
            step = {}
            for item in colour:
                step_value = (item[1] - item[0]) / number_of_steps
                for i in range(0,number_of_steps):
                    if i not in step:
                        step[i] = '#'
                    step[i] = step[i] + '%02x' % round(item[0] + step_value * i)
                    
            step[number_of_steps] = end_colour
            
        return step
        
    @staticmethod
    def get_all_sample_metadata(): 
        conn_string = config['psycopg2_conn_string']
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cursor.execute("select * from biosamples_metadata;")
        # retrieve the records from the database
        result_ds = cursor.fetchall()
        cursor.close()
        conn.close()
        return result_ds


    
    @staticmethod
    def setup_all_sample_metadata(): #CRITICAL-2
        result = Stemformatics_Expression.get_all_sample_metadata()    
        all_sample_metadata = {}
        for row in result:
            if row['chip_type'] not in all_sample_metadata:
                all_sample_metadata[row['chip_type']] = {}
            if row['chip_id'] not in all_sample_metadata[row['chip_type']]:
                all_sample_metadata[row['chip_type']][row['chip_id']] = {}
            
            if row['ds_id'] not in all_sample_metadata[row['chip_type']][row['chip_id']]:
                all_sample_metadata[row['chip_type']][row['chip_id']][row['ds_id']] = {}
            
            all_sample_metadata[row['chip_type']][row['chip_id']][row['ds_id']][row['md_name']] = row['md_value']
            
        return all_sample_metadata
        # now update the g.all_sample_metadata


    @staticmethod
    def get_dataset_sample_metadata(db,ds_id): #CRITICAL-2
        conn_string = config['psycopg2_conn_string']
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cursor.execute("select * from biosamples_metadata where ds_id = %(ds_id)s;",{"ds_id":ds_id})
        # retrieve the records from the database
        result_ds = cursor.fetchall()
        cursor.close()
        conn.close()
        return result_ds


        
    @staticmethod
    def setup_dataset_sample_metadata(db,all_sample_metadata,ds_id): #CRITICAL-2
        result = Stemformatics_Expression.get_dataset_sample_metadata(db,ds_id)    
        identifiers_used = [] 
        for row in result:

            identifier = str(row['chip_type']) + "|" + str(row['chip_id']) + "|" + str(ds_id)

            if row['chip_type'] not in all_sample_metadata:
                all_sample_metadata[row['chip_type']] = {}
            if row['chip_id'] not in all_sample_metadata[row['chip_type']]:
                all_sample_metadata[row['chip_type']][row['chip_id']] = {}
            

            if identifier not in identifiers_used:
                all_sample_metadata[row['chip_type']][row['chip_id']][row['ds_id']] = {}
                identifiers_used.append(identifier) 
                
            all_sample_metadata[row['chip_type']][row['chip_id']][row['ds_id']][row['md_name']] = row['md_value']
            
        file_name =  config['all_sample_metadata_cpickle_file']

        f = open(file_name, 'wb')
        f.write(json.dumps(all_sample_metadata))
        f.close()
        return all_sample_metadata
        # now update the g.all_sample_metadata
     
    @staticmethod    
    def get_assay_platform_graph_info_from_ds_id(db,ds_id):
        db.schema = 'public'
        ds = db.datasets

        datasetMetadata = Stemformatics_Dataset.getExpressionDatasetMetadata(db,ds_id,uid,show_limited)
        if datasetMetadata == None:
            return 1
        
        species = datasetMetadata['species']
        
        result['detectionThreshold'] = datasetMetadata['detection_threshold']
        result['medianDatasetExpression'] = datasetMetadata['median_dataset_expression']
        result['datasetLimitSortBy'] = datasetMetadata['limit_sort_by']
        result['orderBySampleType'] = json.dumps(datasetMetadata['sampleTypeDisplayOrder'].split(','))
        
        # pick the first one if not found
        datasetLimitSortByArray = result['datasetLimitSortBy'].split(',')
        
        if sortBy is None:
            sortBy = "sortBySample Type"
        
        
        tempSortBy = sortBy.replace('sortBy','')    
        if tempSortBy not in datasetLimitSortByArray:
            sortBy = "sortBySample Type"
        
        result['sortBy'] = sortBy
    
        result['lineGraphOrdering'] = lineGraphOrdering= datasetMetadata['lineGraphOrdering']        
        result['lineGraphColours'] = json.dumps(Stemformatics_Expression.return_sample_type_display_group_colours(lineGraphOrdering))
        
        sampleTypeDisplayGroups = datasetMetadata['sampleTypeDisplayGroups']        
        result['sampleTypeDisplayGroupColours'] = json.dumps(Stemformatics_Expression.return_sample_type_display_group_colours(sampleTypeDisplayGroups))
        try:

            result['sampleTypeDisplayGroups'] = json.dumps(eval(datasetMetadata['sampleTypeDisplayGroups']))
        except:
            return 2
        
        
        return result

    @staticmethod    
    def get_assay_platform_graph_info_from_ds_id(db,ds_id): #CRITICAL-2
        try:
            db.schema = 'public'
            ds = db.datasets
            
            result_ds = ds.filter(ds.id==ds_id).one()
            
            if result_ds is None:
                return None
            
            ds_md = db.dataset_metadata
            result_ds_md = ds_md.filter(and_(ds_md.ds_id==ds_id,ds_md.ds_name=='maxGraphValue')).one()

            max_graph_value = float(result_ds_md.ds_value) 

            ap = db.assay_platforms
            result = ap.filter(ap.chip_type == result_ds.chip_type).one()
            
            min_y_axis = result.min_y_axis
            
            if result_ds.min_y_axis != '':
                min_y_axis = result_ds.min_y_axis
            
            result_data = {'max_graph_value': max_graph_value,'min_y_axis': min_y_axis, 'y_axis_label':result.y_axis_label, 'y_axis_label_description':result.y_axis_label_description}
            
            return result_data
        except:
            return  {'max_graph_value': 30,'min_y_axis': 0, 'y_axis_label': '', 'y_axis_label_description':''}

    @staticmethod
    def get_cumulative_sample_labels(ds_id):
        r_server = redis.Redis(unix_socket_path=config['redis_server'])
        delimiter = config['redis_delimiter']
        
        label_name = 'cumulative_labels'+delimiter+str(ds_id)
        try:
            label_names = r_server.get(label_name).split(delimiter)
            return label_names
        except:
            return None
   

    @staticmethod
    def get_sample_labels(ds_id):
        r_server = redis.Redis(unix_socket_path=config['redis_server'])
        delimiter = config['redis_delimiter']
        
        label_name = 'gct_labels'+delimiter+str(ds_id)
        try:
            label_names = r_server.get(label_name).split(delimiter)
            return label_names
        except:
            return None
   
    @staticmethod
    def get_expression_rows(ds_id,probe_list):
        r_server = redis.Redis(unix_socket_path=config['redis_server'])
        delimiter = config['redis_delimiter']
                
        result = {}
        for probe in probe_list:
            temp_row = r_server.get('gct_values'+delimiter+str(ds_id)+delimiter+probe)
            if temp_row is not None:
                row = temp_row.split(delimiter)
                result[probe] = row
        return result
    
 
    @staticmethod
    def get_cumulative_rows(ds_id,probe_list):
        r_server = redis.Redis(unix_socket_path=config['redis_server'])
        delimiter = config['redis_delimiter']
                
        result = {}
        for probe in probe_list:
            temp_row = r_server.get('cumulative_values'+delimiter+str(ds_id)+delimiter+probe)
            if temp_row is not None:
                row = temp_row.split(delimiter)
                result[probe] = row
        return result
    
    @staticmethod
    def get_standard_deviation(ds_id,chip_id,probe_id):
        r_server = redis.Redis(unix_socket_path=config['redis_server'])
        delimiter = config['redis_delimiter']
                
        row_name = str('standard_deviation'+delimiter+str(ds_id)+delimiter+chip_id+delimiter+probe_id)
        sd_value = r_server.get(row_name)
        #~ raise Error
        if sd_value is None:
            sd_value = 0
            
        return float(sd_value)
    
    
    @staticmethod
    def validate_replicate_group_id(db,tests_to_perform): #CRITICAL-2
        output = ''
        
        db.schema = 'public'
        bs_md = db.biosamples_metadata
        ds = db.datasets

        chip_id_table = {}

        result_bs_md = bs_md.filter(bs_md.md_name == 'Replicate Group ID').all()
        for item in result_bs_md:
            ds_id = int(item.ds_id)
            chip_id = item.chip_id

            if ds_id not in chip_id_table:
                chip_id_table[ds_id] = {}

            if chip_id not in chip_id_table[ds_id]:
                chip_id_table[ds_id][chip_id] = 'b'
        
        
        result_ds = ds.all()
        problem_datasets = 0
        list_of_problem_datasets = ''
        problem_datasets_list = []
        for dataset in result_ds:
            ds_id = dataset.id

            # get the gct 
            
            gct_labels = Stemformatics_Expression.get_sample_labels(ds_id)
            # Name and description two first labels
            
            if gct_labels is not None: 
                for chip_id in gct_labels:
                    if chip_id != 'Description' and chip_id != 'NAME':
                        if ds_id not in chip_id_table:
                            chip_id_table[ds_id] = {}

                        if chip_id not in chip_id_table[ds_id]:
                            chip_id_table[ds_id][chip_id] = 'g'
                        else:    
                            chip_id_table[ds_id][chip_id] = chip_id_table[ds_id][chip_id] + 'g'
            else:
                if ds_id not in problem_datasets_list:
                    list_of_problem_datasets = str(ds_id)+","+list_of_problem_datasets
                    problem_datasets += 1
                    problem_datasets_list.append(ds_id)
                

            if tests_to_perform == 'bgc':
                # get the cumulative
                
                cumulative_labels = Stemformatics_Expression.get_cumulative_sample_labels(ds_id)

                # Name and description two first labels
                if cumulative_labels is not None: 
                    for chip_id in cumulative_labels:
                        if chip_id != 'Description' and chip_id != 'NAME' and chip_id != 'Probe':
                            if ds_id not in chip_id_table:
                                chip_id_table[ds_id] = {}

                            if chip_id not in chip_id_table[ds_id]:
                                chip_id_table[ds_id][chip_id] = 'c'
                            else:    
                                chip_id_table[ds_id][chip_id] = chip_id_table[ds_id][chip_id] + 'c'
                else:
                    if ds_id not in problem_datasets_list:
                        list_of_problem_datasets = str(ds_id)+","+list_of_problem_datasets
                        problem_datasets += 1
                        problem_datasets_list.append(ds_id)
 
        count_datasets = 0
        
        for ds_id in chip_id_table:
            issue = False
            count_datasets += 1 
            for chip_id in chip_id_table[ds_id]:
                value = chip_id_table[ds_id][chip_id] 
                if value != tests_to_perform:
                    output = output  + str(ds_id) + "|" + chip_id.decode('utf-8') + "|" + value + "\n "
                    issue = True
            if issue:
                if ds_id not in problem_datasets_list:
                    list_of_problem_datasets = str(ds_id)+","+list_of_problem_datasets
                    problem_datasets += 1
                    problem_datasets_list.append(ds_id)
        
        output = output + str(problem_datasets) + "/" + str(count_datasets) + "\n"
        output = output + list_of_problem_datasets + "\n"
        return output




    @staticmethod
    def return_sample_type_display_group_colours(sampleTypeDisplayGroups):
        if sampleTypeDisplayGroups is None:
            return []
        newColourArray = ['#CC0066','#000099','#006600','#666600','#0AD3EF','#82DC27','#FDD50C','#FD0CF4','#783255','#11A81A','#E99F19','#19E9AC','#4119E9','#B219E9','#E05B50','#EEEA48', '#A72E71', '#AB5090', '#ABCD34', '#AC4401', '#AD1049', '#B2A139', '#B42DA1', '#B4E0B8', '#B5D2BC', '#B937B7', '#B9D0E6', '#BBF90E', '#BE5415', '#C43C25', '#C6C9BE', '#CAB861', '#CDAAB9', '#D0490F', '#D09F85', '#D34AE9', '#D794D7', '#DC93C1', '#DE3C93', '#E2F8E1', '#E3EE73', '#E52700', '#E5B9DE', '#EC3198', '#EF4DDD', '#EFDE92', '#F04874', '#F0A4E1', '#F31AEC', '#F45CC4', '#F4C928', '#F8FC9E', '#F9DCE9', '#FE466C']

        data = json.loads(sampleTypeDisplayGroups)
        group_members = {}
        return_stepped_colours = {}
        for sample_type in data:
            group = data[sample_type]
            if group not in group_members:
                group_members[group] = 0
            group_members[group] = group_members[group] + 1
        for group in group_members:
            start_colour = newColourArray[group]
            end_colour = '#000000'
            list_of_start_end_colours = [[start_colour,end_colour]]
            number_of_steps = group_members[group]
            return_stepped_colours[group] = Stemformatics_Expression.return_stepped_colours(list_of_start_end_colours,number_of_steps)
        return return_stepped_colours


    @staticmethod
    def return_binning_range(start,end,step_value,value):    
        top = start + step_value
        bottom = start
        step_count = 0
        while value > top:
            top = top + step_value
            bottom = bottom + step_value
            step_count += 1
        step_string = str(bottom)+' - '+ str(top)
        return [step_string,step_count]

    @staticmethod
    def return_gct_file_sample_headers_as_replicate_group_id(db,ds_id,chip_ids,remove_chip_ids,gct_header="NAME\tDescription"):
        mapping = Stemformatics_Dataset.get_chip_id_to_replicate_group_id_mappings(db,ds_id)
        if chip_ids is not None:
            for chip_id in chip_ids:
                if chip_id not in remove_chip_ids: 
                    replicate_group_id = mapping[chip_id] 
                    gct_header +="\t"+replicate_group_id
            gct_header +="\n"
        else:
            gct_header +="\tIssue with finding chip_ids. Please contact the "+c.site_name+" team.\n"
        return gct_header


    @staticmethod
    def return_sample_details(db,ds_id):
        chip_ids = Stemformatics_Expression.get_sample_labels(ds_id)
        mapping = Stemformatics_Dataset.get_chip_id_to_replicate_group_id_mappings(db,ds_id)
        sample_details = {} 
        for chip_id in chip_ids:
            replicate_group_id = mapping[chip_id] 
            sample_details[chip_id] = {'replicate_group_id':replicate_group_id}
        return sample_details


    """
    Returns a True or False

    It saves a list of json values into redis. It saves the full_data and the sample_data.
    First it checks if the value is there (sample_data) and if it's there it returns True.
    Secondly, if it's not there, it will build it from scratch and then store it in Redis.

    db_id is integer
    uid is integer
    ensemblID has to be unique
    all_sample_metadata is from the global
    role is admin,annotator,normal or None

    """
    @staticmethod
    def return_yugene_graph_data(db_id,uid,ensembl_id,all_sample_metadata,role):

        # first check that this doesn't already exist in redis
        sample_values = Stemformatics_Expression.get_yugene_sample_data_graph_values(uid,ensembl_id,db_id)
        if sample_values is not None and sample_values != '[]':
            return True

        datasets_dict = Stemformatics_Dataset.get_all_x_platform_datasets_for_user(uid,db_id,role)

        # graph_values has two keys, full_data and sample_data returned
        graph_values = Stemformatics_Expression.return_x_platform_matricks_data(db_id,datasets_dict,ensembl_id,all_sample_metadata)

        # set both full_data and sample_data
        result = Stemformatics_Expression.set_yugene_graph_values(uid,ensembl_id,db_id,graph_values)
        return result

    """
    Asks for metadata_list of metadata names that we want to take back eg. tissue
    all_sample_metadata from startup
    values from the filtered data
    returns dictionary
    """
    @staticmethod
    def return_breakdown_of_filtered_results(metadata_list,all_sample_metadata,values):
        breakdown_dict = {}
        for metadata in metadata_list:
            breakdown_dict[metadata] = {}

        if 'ds_id' in metadata_list:
            breakdown_dict['ds_id'] = {}

        for row in values:
            chip_id = row[2]
            ds_id = row[3]
            chip_type = row[4]
            sample_metadata_values = all_sample_metadata[chip_type][chip_id][ds_id]

            if 'ds_id' in metadata_list:
                if ds_id not in breakdown_dict['ds_id']:
                    breakdown_dict['ds_id'][ds_id] = 1
                else:
                    breakdown_dict['ds_id'][ds_id] += 1
         

            for metadata in metadata_list:
                try:
                    value = sample_metadata_values[metadata]

                    if value not in breakdown_dict[metadata]:
                        breakdown_dict[metadata][value] = 1
                    else:
                        breakdown_dict[metadata][value] += 1
                except:
                    pass

        return breakdown_dict

    """
    Gets the full data and filters it based on the filters variable.
    Currently only filtering on ds_id and start/end value
    Later will use filter on "search" term

    Returns a cut down list of lists or and error text if stated in max_length_action
    Expects the order of the values in the list to be [value,probe_id,chip_id,ds_id,chip_type]


    filters are the filter types (see below)
    full_data is the full data from redis
    max_length is the max # of items eg. 100 or None for no limit
    max_length_action is the action to take when finding over the length
        options are 'truncate' or 'error'

    eg. filters = {'filter_value_start':0,'filter_value_end':0.1} # start must have an end
    eg. filters = {'ds_ids':[1000,2000]} # multiple dataset ids supported 
    eg. filters = {'filter_value_start':0,'filter_value_end':0.1,'ds_ids':[1000]} # search for all three at the same time
    eg. filters = {'search':'human and ipsc'} # not implemented yet

    """
    @staticmethod
    def filter_yugene_graph(filters,db_id,full_data,max_length,max_length_action='error'):
        filtered_result = full_data

        # no filters means we should return empty
        if filters is None or filters == '{}' or filters =='':
            return []

        if full_data is None:
            return None

        if filters is not None and 'ds_ids' in filters:
            ds_ids = filters['ds_ids']
            if isinstance(ds_ids,list):
                filtered_result= [x for x in filtered_result if x[3] in ds_ids]


        if filters is not None and 'filter_value_start' in filters and 'filter_value_end' in filters:
            try:
                filter_value_start = float(filters['filter_value_start'])
                filter_value_end = float(filters['filter_value_end'])
                if isinstance(filter_value_start, float) and  isinstance(filter_value_end, float):
                    filtered_result = [x for x in filtered_result if x[0] >= filter_value_start and x[0] <= filter_value_end]
            except:
                pass 


        # option of cutting off the length or returning a text with an error - default is error
        if max_length is not None:
            actual_length = len(filtered_result)
            if actual_length > max_length:
                if max_length_action == 'error':
                    error_text =  "Error as the actual length "+str(actual_length) + " is greater than the cutoff "+ str(max_length)
                    return error_text
                if max_length_action == 'truncate':
                    filtered_result = filtered_result[0:max_length] 

        return filtered_result



