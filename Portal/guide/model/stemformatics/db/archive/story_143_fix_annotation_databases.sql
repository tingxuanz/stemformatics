update annotation_databases set genome_version = 'Mus musculus' where genome_version = 'Mus Musculus';

update annotation_databases set genome_version = 'Homo sapiens' where genome_version = 'Homo Sapiens';
