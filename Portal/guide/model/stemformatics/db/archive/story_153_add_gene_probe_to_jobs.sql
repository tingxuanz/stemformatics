-- Story #153 add gene and probe columns as text to jobs table

ALTER TABLE stemformatics.jobs ADD COLUMN gene text;
ALTER TABLE stemformatics.jobs ADD COLUMN probe text;
