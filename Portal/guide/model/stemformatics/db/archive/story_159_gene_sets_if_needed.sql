-- WARNING
-- WARNING
-- WARNING
-- WARNING
-- WARNING
-- WARNING
-- WARNING

-- this is to be used only if you cannot upload tsv because uid is before description
alter table stemformatics.gene_sets add column new_uid bigint;
update stemformatics.gene_sets set new_uid = uid;
alter table stemformatics.gene_sets drop column uid;
alter table stemformatics.gene_sets rename column new_uid to uid;
