-- SQL upgrades for v3.3.1

-- Story #179 share gene set
CREATE TABLE stemformatics.shared_resources (
    share_type TEXT NOT NULL,
    share_id INTEGER NOT NULL,
    from_uid INTEGER NOT NULL,
    to_uid INTEGER NOT NULL,
    created timestamp without time zone default now(),
    expiry_date timestamp without time zone default NULL,
    PRIMARY KEY (share_type, share_id,from_uid,to_uid)
);

CREATE INDEX to_uid_index on stemformatics.shared_resources (to_uid);
CREATE INDEX from_uid_index on stemformatics.shared_resources (from_uid);

CREATE TABLE stemformatics.notifications (
    id serial NOT NULL,
    notify_type TEXT NOT NULL,
    uid TEXT NOT NULL,
    subject TEXT NOT NULL,
    body TEXT NOT NULL,
    unread boolean default TRUE,
    creation_date timestamp without time zone default NULL,
    PRIMARY KEY (id)
);
CREATE INDEX uid_index on stemformatics.notifications (uid);


-- Story #181 Admin page
ALTER TABLE stemformatics.users ADD COLUMN is_admin boolean NOT NULL default False;
