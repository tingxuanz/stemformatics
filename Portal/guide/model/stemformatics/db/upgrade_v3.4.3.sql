
alter table assay_platforms add platform_type TEXT NOT NULL DEFAULT 'microarray';

-- Example 'HTS' (high throughput sequencer) type of assay platform:
-- insert into assay_platforms (chip_type,species,manufacturer,platform,version,platform_type) values (100,'Mouse','Life Technologies','SOLiD 5500xl','NCBIM37 Gene','HTS');

update assay_platforms set platform_type='microarray' where platform_type != 'HTS';

