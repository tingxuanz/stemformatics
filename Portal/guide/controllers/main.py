import logging,base64
log = logging.getLogger(__name__)
from pylons import config,request, response, session, tmpl_context as c, url

from pylons.controllers.util import abort, redirect

from guide.lib.base import BaseController, render

from guide.model.stemformatics import *

class MainController(BaseController):
    __name__ = 'MainController'
    
    def __before__(self): #CRITICAL-3
        super(MainController, self).__before__ ()
 
    def health_check(self):
        result = Stemformatics_Admin.health_check(db)
        return result

    def tests(self):
        return render('tests.mako')

    def suggest_dataset(self):
        redirect(config['agile_org_base_url']+'datasets/external_add')
    
    def export(self):
        # Task #396 - error with ie8 downloading with these on SSL
        del response.headers['Cache-Control']
        del response.headers['Pragma']
        
        response.headers['Content-type'] = 'text/csv'
        stemformatics_version = config['stemformatics_version']
        #This Content-Disposition is from teh table2CSV file so am leaving this as csv. Might change to tsv later
        response.headers['Content-Disposition'] = 'attachment;filename=export_stemformatics_'+stemformatics_version+'.csv'
        response.charset= "utf8"
        data = request.params['exportdata']
        return data


    """
        must be logged in to use this

        data - always in svg_xml as show in javascript below:

        example: Portal/guide/public/js/msc_signature/rohart_msc_test.js
        var tmp = document.getElementById(this_div);
        var svg = tmp.getElementsByTagName("svg")[0];
        // Extract the data as SVG text string
        var svg_xml = (new XMLSerializer).serializeToString(svg);
 
        file_name: filename of the download file without the extension (calculated by output_format)
        output_format: restrict to svg/pdf/png
        

        NOTE: not sure about allowing download if you are registered only
    """
    #@Stemformatics_Auth.authorise(db)
    def export_d3(self):

        """
        available = Stemformatics_Auth.check_real_user(c.uid)
        if not available: 
            return_message = "This could not be downloaded. You need to be logged in with your own email address to be able to use this feature."
            return return_message     
        """
 
        data = request.params['data']
        file_name = request.params['file_name']
        output_format = request.params['output_format']
       
        export_data = Stemformatics_Export.get_export_data_for_d3(data,file_name,output_format)

        del response.headers['Cache-Control']
        del response.headers['Pragma']
        
        response.headers['Content-type'] = export_data.content_type
        response.headers['Content-Disposition'] = 'attachment;filename='+export_data.file_name
        response.charset= "utf8"

        return export_data.data
       
        
    def send_email(self):

        sender  = config['from_email']
        recipient = request.params.get('to_email')
        subject = request.params.get('subject')
        body = request.params.get('body')

        available = Stemformatics_Auth.check_real_user(c.uid)
        if not available: 
            return_message = "This email could not be sent. You need to be logged in with your own email address to be able to share links."
            return return_message     
  
        success =  Stemformatics_Notification.send_email(sender,recipient,subject,body)    
        try:
            if success:
                result = "This email was sent successfully."
            else:
                result = "This email was not sent."
                
        except:
            result = "This email was not sent."

        return result

 
    # Trying to fix issues with ie9 and downloading canvas2image files
    def save_image(self):
        del response.headers['Cache-Control']
        del response.headers['Pragma']
        
        response.headers['Content-type'] = 'image/png'
        response.headers['Content-Disposition'] = 'attachment;filename=download_image.png'
        encoded = request.params.get('imgdata')
        encoded = encoded.replace(' ','+') 
        decoded = base64.b64decode(encoded)
        return decoded

    def download_thomson_reuters_xml_file(self):
        result = Stemformatics_Dataset.get_thomson_reuters_feed()

        file_text = Stemformatics_Dataset.create_thomson_reuters_xml_file(result)

        response.headers['Content-type'] = 'text/xml'
        response.headers['Content-Disposition'] = 'filename=stemformatics_thomson_reuters_feed.xml'
        response.charset= "utf8"
        return file_text 
  
