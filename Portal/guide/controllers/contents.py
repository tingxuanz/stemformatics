import logging

log = logging.getLogger(__name__)

from pylons import request, response, session, tmpl_context as c, url
from pylons import config
from pylons.controllers.util import abort, redirect

from guide.lib.base import BaseController, render

from guide.model import twitter
from guide.model.stemformatics import *
from guide.model.error import GeneralS4MError

class ContentsController(BaseController):
    def homepage(self):
        redirect(url('/contents/index'))

    def index(self):
        c.header_selected = ''
        c.speed_up_page = 'true'
        c.title = c.site_name+" - Find expression data from leading stem cell laboratories in a format that is easy to search, easy to visualise and easy to export"
        try:
            number = 3
            force_refresh = False
            result= twitter.get_recent_tweets(number,force_refresh)
            c.tweets = result[0]
        except:
            c.tweets = []

        c.number_of_public_samples = Stemformatics_Dataset.get_number_public_samples(db)        
        c.number_of_public_datasets = Stemformatics_Dataset.get_number_of_datasets(db)['Public']
        return render ('/contents/index.mako')
    
    def about_us(self):
        c.title = c.site_name+" - About Us"
        return render ('/contents/about_us.mako')

    def speed_test(self):
        c.title = c.site_name+" - Speed Test"
        return render ('/contents/speed_test.mako')

    def our_data(self):
        c.title = c.site_name+" - Our Data"
        return render ('/contents/our_data.mako')


    def our_publications(self):
        c.title = c.site_name+" - Our Publications"
        c.data_publications = Stemformatics_Dataset.get_data_publications()
        return render ('/contents/our_publications.mako')

    def browser_compatibility(self):
        c.title = c.site_name+" - Browser Compatibility"
        return render ('/contents/browser_compatibility.mako')

    def faq(self):
        c.title = c.site_name+" - FAQ"
        return render ('/contents/faq.mako')

        
        
    def help(self):
        redirect(url('/contents/faq'))
        
    def contact_us(self):
        c.title = c.site_name+" - Contact Us"
        return render ('/contents/contact_us.mako')

    def privacy_policy(self):
        c.title = c.site_name+" - Privacy Policy"
        return render ('/contents/privacy_policy.mako')
        
    def disclaimer(self):
        c.title = c.site_name+" - Disclaimer"
        return render ('/contents/disclaimer.mako')

    def hamlet(self):
        redirect(url('/contents/removal_of_hamlet'))
    
    def download_mappings(self): 
        c.title = c.site_name+" - Download Probe Mappings"
        c.results = Stemformatics_Dataset.get_assay_platform_list(db)
        return render ('/contents/download_mappings.mako')

    def removal_of_hamlet(self):
        c.message = "After some deliberation, we have decided to remove the Hamlet Interactive Hierarchical Clustering as of v5.0.3 due to stability issues.  We apologise for the inconvenience.  Hierarchical Clusters can still be produced via the Analyses tab."
        c.title = "Removal of Hamlet" 
        return render('workbench/error_message.mako')

    def registration_submitted(self):
        c.message = "Thank you for registering! Please confirm your registration by following instructions in your confirmation email."
        c.title = "Registration submitted"
        return render('workbench/error_message.mako')




    def removal_of_comparative_marker_selection(self):
        #redirect(url('/workbench/removal_of_comparative_marker_selection'))
               
        c.message = "After some deliberation, we have decided to remove the Comparative Marker Selection as of v4.1.  The main reason was that this particular implementation was unstable due to a number of reasons.  We apologise for the inconvenience. "
        c.title = "Removal of Comparative Marker Selection" 
        return render('workbench/error_message.mako')


