#!/bin/bash
WORKING_DIR=/data/repo/git-working/stemformatics/Portal
NOSETESTS_DIR=/data/repo/git-working/stemformatics/

source /var/www/pylons/virtualenv/bin/activate
cd $WORKING_DIR

nosetests --with-pylons=qfab-production-pglocal.ini --pdb-failures $1


