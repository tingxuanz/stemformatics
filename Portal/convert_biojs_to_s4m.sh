#!/bin/bash

biojs_dir=/home/rowlandm/biojs2
s4m_dir=/home/rowlandm/s4m

choose_option () {

    echo -n "$1"
    read answer
    if [ "$answer" == 1 -o "$answer" == 2 ]; then
        return "$answer"
    else
        echo "Error in answering the question. Please run again"
        exit 1
    fi
}

choose_option "Select 1) biojs to s4m or 2) s4m to biojs: "
echo "$answer"

if [ "$answer" == 1 ]; then
    echo "biojs to s4m selected"
    cp ${biojs_dir}/lib/index.js ${s4m_dir}/Portal/guide/public/js/msc_signature/rohart_msc_graph.js
fi

if [ "$answer" == 2 ]; then
    echo "s4m to biojs selected"

fi



