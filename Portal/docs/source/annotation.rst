.. annotation classes

.. automodule:: annotation
    :members:
    :inherited-members:

.. automodule:: catalog
    :members:
    :inherited-members:
