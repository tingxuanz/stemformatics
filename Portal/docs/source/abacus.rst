=================================================
ABACUS: Activity-Based Access and Usage Superisor
=================================================

 * Nick Seidenman (Molecular Medicine Division, WEHI)

 `He who can destroy a thing, controls a thing.`  Paul Atreides, aka Maud D'ib, ''Dune''

.. include:: <isolat1.txt>

.. |godel| unicode:: G U+000F6 del

This paper describes a system for defining and managing entities and permissions ascribed 
thereto for the purpose of controlling access to other, possibly disjoint entities such as 
data stores or applications.  It is similar to the XACML hierarchical role-based control profile, however it is not based on it.  It also bears some resemblance to the 
`RBAC proposal` 
submitted earlier in this project.   The thing that distinguishes this system from most others is that it focuses on managing access to activities, which in turn may or may not perform operations on some resource (e.g. data store) somewhere.  This may seem odd at first blush, but it follows naturally from the idea that access is itself an activity and it is various forms of that activity (STORE, FETCH, DELETE) that we want to control.  Whereas these examples explicitly involve a data store of some sort, access also encompasses EXECUTE activities that may involve no data store at all.  The nomenclature and inherent function of permissions within this system follow naturally as well since they are created from the perspective of "permission to DO" rather than "permission to HAVE" or "permission to SEE".  In short, it is a simpler approach that turns out to be more powerful and flexible than an object-centric access control system.

Controlled Access Domain
========================

Having an access/activity control system is pointless unless there is
something to which such access should be controlled.  For the purposes
of this discussion this will be referred to as a ''Controlled Access
Domain'' or DAC, to save typing keystrokes.  One of the advantages of
this system is that the DAC need not be tightly-integrated with this
activity controller.  Databases, for example, can be managed
separately from a web portal that implements the activity controller
and, not withstanding permissions granted to portal subjects, the
database's DBA may choose to further (or completely) restrict access
thereto.

Entities
========

The components that make up this system are few and the relationships
between them are simple.  The diagram, below, depicts these entities
and relationships.

.. image:: erd.png

Activity
--------

An activity is a process or procedure launched, carried out,
committed, foisted or otherwise instigated by a subject for the
purpose of carrying out some task.  This may include accessing,
storing, updating or deleting records in a data store, launching an
application, or managing the ACS itself.

Activities are said to be `undertaken` by a :ref:`Subject`


Subject
-------

Subjects initiate and/or undertake activities.  Subjects provide an
abstract means of identifying persons or automata that conduct
operations, that is, :ref:`activities <Activity>` on or within the
:ref:`CAD <ControlledAccessDomain>`.  This may included such administrative
operations as adding, modifying or removing subjects and permissions.

Each subject has a unique, non-negative integer ID associated with it.
The data sets that manage subject information can (should) include
log-in information such as username and password, or OpenID
credentials.

Subjects are assigned :ref:`roles <Role>`s which effectively grants
them the permissions attributed to those roles.  When attempting to
engage in an activity, the activity dispatch logic will query the
subject to see if it has the requisite permission(s) to undertake that
activity.  (See 
:ref:`Activity Clearance <ActivityClearance>`)

Group
-----

A group is nothing more than a collection of subjects.  A role can be
assigned to a group which effectively assigns the role to all members
of the group.

Role
----

A role is little more than a collection of :ref:`permissions
<Permission>`.  Roles are `assigned` to subjects and they are
`associated` with :ref:`activities <Activity>`. In the context of
activities, roles enumerate the permissions required to undertake an
activity.  In the context of subjects, roles enumerate the permissions
that have been assigned (i.e., granted) to a subjected.  (See
:ref:`Activity Clearance` <ActivityClearance>`

Permission
----------

Before a 
:ref:`subject <Subject>` can undertake an activity it must be
authorized or ''cleared'' to do so.  Each permission is signified by a
mnemonic token that provides some (scrutable) indication as to what
permission is being granted, such as CREATE_SUBJECT, or RUN_TOOL.
Permissions are ascribed to subjects via :ref:`role <Role>` assignment and
they are ascribed to activities via role association.

Permission Set
..............

A permission set is merely the collection of permissions ascribed to a
:ref:`role <Role>` or a :ref:`subject <Subject>`.  Permission sets come into use
for determining :ref:`activity clearance <ActivityClearance>` in which
permissions required are checked against permissions granted.  If the
:ref:`activity clearnace optimization <Pre-calculatedRole` method,
described below, is employed, a permission set can be represented by a
single, non-zero scalar.

Role Association
================

Roles are said to be ''associated'' with
:ref:`activities <Activity>`.  This
association establishes a set of one or more 
:ref:`permissions <Permission>`
that must be granted to a subject before it can undertake that
activity.  This does not, however, directly ascribe permissions to the
activity.  Consequently, if the role's permissions change, the
permissions required to undertake the activity under the auspices of
this role change, too.  Activities may have more than one role
associated with them.  Each role represents a separate and distinct
set of permissions required to undertake the activity.  Satisfying all
the permissions in any one of the activities associated roles is
sufficient to grant clearance to a subject.

Role Assignment
===============

:ref:`Roles <Role>` are assigned to :ref:`subjects <Subject>`.  When a
subject has been assigned a role it has effectively been granted all
of the :ref:`permissions <Permission>` ascribed to that role.  The
permissions per se remain attributes of the role and are not otherwise
transferred or ascribed directly to the subject.

A subject's 
:ref:`permission set <PermissionSet>` is the amalgam of all
permissions granted across all roles assigned to the subject.

Activity Clearance
==================

For a :ref:`subject <Subject>` to undertake an :ref:`activity <Activity>` it must
first be ''cleared'' to do so.  Clearance is granted when a subject
presents a set of :ref:`permissions <Permission>` that contains all of the
permissions ascribed to at least one of the intended activity's
associated :ref:`roles <Role>`.  The following diagram depicts the the
clearance process:

.. image:: clearance.png

Simply put, once a subject's permission set has been ascertained, the
activity clearance logic iterates through each role associated with
the activity, checking the permissions ascribed to the role against
the permissions in the subject's [#PermissionSet permission set].  If
a role is found in which all of the ascribed permissions are also
found in the subject's permission set, clearance is granted to that
subject.  If the list of associated roles is exhausted before a
matching role permission set is found, clearance is denied.

Note that for each role associated with a given activity the
permissions ascribed to that role are ''checked against the union of
all the permission sets of all roles assigned to the subject.  It
might seem as though simply comparing roles associated with roles
assigned should be sufficient.  However, it is anticipated that there
may be roles associated with activities that have not been explicitly
assigned to a subject, but for which the the subject may be cleared
for an activity by the composition of permissions granted to the roles
assigned.

For example, suppose an activity is associated with role **R1**
which has ascribed to it permissions {**P1**, **P2**, **P3**}.
Further suppose that a subject has been assigned role **R2** with
permissions {**P1**} and is a member of a group that has been
assigned role **R3** with permissions {**P2**, **P3**}.  Neither
the subject nor the group it is a member of have sufficient
permissions, individually, to undertake the activity.  however, the
composition (union) of the permissions of the subject and its
containing group ({**P1**} + {**P2**, **P3**}) are sufficient to
undertake the activity as though the subject had been assigned role
**R1**.

Ascertaining a Subject's Permission Set
=======================================

A subject's 
:ref:`permission set <PermissionSet>` is the union of the 
:ref:`permissions <Permission>` ascribed to any roles the subject has been assigned, including 
roles assigned to :ref:`groups <Group>` of which the subject is a member.  Since groups can themselves 
contain groups, the permissions ascribed to roles assigned to subgroups must also be taken into 
account.  Care must also be taken when adding subjects to groups to ensure no "group loops" -- 
groups containing one or more of their own containing groups, or ancestors -- are created.

Once the groups to which a subject belongs have been determined, it is a simple matter to 
build a list of roles assigned and construct the subject's permission set.

Optimizing Activity Clearance
=============================

Without any sort of optimization, the clearance process can involve anywhere from a few to 
several to many queries against the data sets that maintain subject/group/role/permission information.   
Reducing the number of queries will reduce the overall time and processing required to obtain 
clearance.  In an application such as a web portal many activities -- and, thus, many clearances -- 
may be required in short order, this sort of optimization is highly desirable, if not crucial.

One optimization method, proposed here, is loosely-based on 
|godel| numbers. Given a (perhaps) 
ordered set of symbols, each symbol is assigned a unique prime number.  Any given collection 
of symbols can be uniquely represented by the product of their corresponding symbols.  
Moreover, determining whether another set is a subset of the first one is done by simply 
rendering the second set's |godel| number and seeing whether or not it divides the first set's 
number.  If it does then the second set is a subset of the first.   

If we think of roles as subsets of permissions, we can determine clearance by seeing if 
the |godel| number of a role's permission set divide's the |godel| number of a subject's 
permission set.  If it does, the subject can be granted clearance for that activity as 
that role. Trivially, if a role's permission set product is greater than the subject's, 
it should be obvious the subject has insufficient permission for clearance.

Pre-calculated Role permission_code
-----------------------------------

Since roles are relatively static entities, we can pre-calculate their prime-product 
(which we'll henceforth refer to as the ''permission code'').  When a permission is 
granted to or revoked from a role, the permission code for that role is divided by 
the prime number associated with the permission being granted or revoked to yield a 
new permission code.  In theory, it should be possible to do this for subjects as well, 
though it is less straightforward than for roles.   Whether or not this is implemented will 
depend on typical usage, as yet to be determined.

Context
=======

The term :ref:`context is a component of XACML and is also mentioned
in the [wiki:AccessControlRBACProposal RBAC proposal].  There is,
however, no functional distinction between a ''context'' and a
:ref:`permission <Permission>`.  Context can be regarded as a component of
the [#Subject subject's] permission state.  Notationally, however, the
distinction is worth including as a subclass of permission that can be
used as a sort of ''permission overlay'' covering a broad category of
subjects or :ref:`CADs <ControlledAccessDomain>`.  This might be implemented
as a separate set of attributes that can be managed within a user
account.  For example, there might be two drop-down menus, one for
permissions and a separate one for "contexts".  Internally, they both
perform the same function, but referring to some permissions as
contexts may help clarify when (or, rather, where) a user is allowed
to do certain things.

Thinking of context as a broad and/or minimal permission required to
access a :ref:`CAD <ControlledAccessDomain>` also reduces the number and
type of permissions that must be created and managed.  Likewise a CAD
itself can be represented in this system exactly the same way
permissions are represented.  The mnemonics for permissions, contexts,
and CADs can -- and should -- be different, but all three will still
use numeric codes for internal representation and the *calculus*
of access.

Examples
========

Materials
---------

Suppose there are two databases, which we'll refer to as [#Context
contexts] CAD_D1 and CAD_D2.  We'll assume there are two groups --
**G1** and **G2** -- of subjects (labs, companies, clubs, etc.)
that share access to these CADs but that not all data is available to
all members of both groups.  Thus there will be two contexts, GROUP_G1
and GROUP_G2.  Finally, we'll establish :ref:`permissions <Permission>`
named CREATE_REC, READ_REC, UPDATE_REC, and DELETE_REC.  Putting all
this into a table we have:

+------------+------+------------+
| Mnemonic   | Code | Type       |
+============+======+============+
| CREATE_REC |   2  | Permission |
+------------+------+------------+
| READ_REC   |   3  | Permission |
+------------+------+------------+
| UPDATE_REC |   5  | Permission |
+------------+------+------------+
| DELETE_REC |   7  | Permission |
+------------+------+------------+
| CAD_D1     |  11  | Context    |
+------------+------+------------+
| CAD_D2     |  13  | Context    |
+------------+------+------------+
| GROUP_G1   |  19  | Context    |
+------------+------+------------+
| GROUP_G2   |  23  | Context    |
+------------+------+------------+


Example 1: Simple read access to a database
...........................................

Now we can begin to build roles for accessing the databases.  Recall
that when associated with an activity a role specifies the minimum set
of permissions that a subject must have to undertake the activity.  We
define a role called READ_D1 with permission set {CAD_D1, READ_REC}
which, when associated with an activity, will restrict access to D1 to
only those activities undertaken by a subject that has these
permissions in its permission set.  We'll assume there's an activity
called ACT_READ_REC that can be called with specific database and
query parameters to retrieve records from a database.

In the simplest case we can now assign the role READ_D1 to a subject
**S** and when **S** attempts to undertake ACT_READ_REC it will be
granted a clearance since its permission set is the same as that of
role READ_D1.

Suppose, however, that we create two other roles: READ_ONLY and
ACCESS_D1 which have permission sets {READ_REC} and {CAD_D1},
respectively.  Neither role has sufficient permission to obtain a
clearance for ACT_READ_DB, however, if we assign both roles to
**S**, its permission set will be the ''union'' of these sets and
therefore have the requisite permissions.

Notice, too, that by revoking or de-assigning role ACCESS_D1 from
**S** -- that is, by dividing **S**'s permission code by
ACCESS_D1's permission code -- we revoke any CAD_D1 permission (or
context) that may have been granted through assignment of other roles
as well.

Example 2: TBD
..............


Summary
=======

The ACS system proposed, here, is relatively easy to implement and manage in an 
RDBMS that supports triggers and stored procedures.  An SQL schema segment that 
defines the tables and code will soon be added as an attachment to this page.