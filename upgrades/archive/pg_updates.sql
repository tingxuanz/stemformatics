update assay_platforms set platform_type = 'RNASeq Gene' where chip_type = 100;
update assay_platforms set platform_type = 'RNASeq Transcript' where chip_type = 101;
update assay_platforms set platform_type = 'protein' where chip_type = 105;

ALTER TABLE assay_platforms ADD COLUMN probe_name text NOT NULL default 'Probe';
alter table assay_platforms alter column probe_name set default 'Probe';
update assay_platforms set probe_name = 'Probe';
update assay_platforms set probe_name = 'Gene' where chip_type = 100;
update assay_platforms set probe_name = 'Transcript' where chip_type = 101;
update assay_platforms set probe_name = 'miRNA' where chip_type = 103;
update assay_platforms set probe_name = 'Protein' where chip_type = 105;
update assay_platforms set probe_name = 'DMR' where chip_type = 107;
update assay_platforms set probe_name = 'ChIPSeq Peak' where chip_type = 108;
update assay_platforms set probe_name = 'Transcript' where chip_type = 129;

insert into dataset_metadata values(6128,'top_miRNA','miR-378-3p,miR-205-5p,miR-10b-5p,miR-21-5p,miR-302d-3p,miR-291a-3p');
delete from dataset_metadata where ds_id = 6128 and ds_name = 'top_miRNA';

insert into dataset_metadata values(6117,'PG_type','PGTranscriptome');
insert into dataset_metadata values(6126,'PG_type','PGTranscriptome');
insert into dataset_metadata values(6127,'PG_type','PGProteome');
insert into dataset_metadata values(6128,'PG_type','PGTranscriptome');
insert into dataset_metadata values(6130,'PG_type','PGProteome');
insert into dataset_metadata values(6131,'PG_type','PGChromatin');
insert into dataset_metadata values(6138,'PG_type','PGTranscriptome');
insert into dataset_metadata values(6139,'PG_type','PGTranscriptome');
insert into dataset_metadata values(6149,'PG_type','PGChromatin');
insert into dataset_metadata values(6150,'PG_type','PGChromatin');
insert into dataset_metadata values(6151,'PG_type','PGChromatin');
insert into dataset_metadata values(6197,'PG_type','PGTranscriptome');

ALTER TABLE assay_platforms ADD COLUMN default_graph_title text NOT NULL default 'Gene Expression Graph';
update assay_platforms set default_graph_title = 'miRNA Expression Graph' where chip_type = 103;
update assay_platforms set default_graph_title = 'Protein Expression Graph' where chip_type = 105;
update assay_platforms set default_graph_title = 'DNA Methylation' where chip_type = 107;
update assay_platforms set default_graph_title = 'Histone ChIPSeq Peaks' where chip_type = 108;


  
