./portal-admin:
	Stem Cell Portal admin bash script. Run without args for usage info.

## NOTE: Not currently used, follow install procedure on wiki.
./pylons-install:
	Set up a new Pylons instance and install required Python modules.
	Expects (or creates) a Python virtual environment to install into.
	NOTE: Interactive script, takes no args.
