## ==========================================================================
## portal-admin profile script - QFAB Local DB
## 
## This file is 'source'd by the "portal-admin" script and takes bash syntax.
##
## Variables defined here are available in "portal-admin" functions.
##
## ==========================================================================

## The repository path containing the Pylons source to deploy from.
REPO_SRC="/var/local/repo/git-working/stemformatics"

## The path to publish a Pylons instance (a new subdirectory will be created
## matching the "config_name" argument to this script, if it doesn't already
## exist).
PYLONS_BASE="/var/www/pylons"

## The path to the Python virtual environment in which Pylons and related
## python modules are installed.
VIRTUALENV="$PYLONS_BASE/virtualenv"

## Path to the python executable which will launch the pylons instance.
VPYTHON="$VIRTUALENV/bin/python"

## Valid pylons instance names. Space-separated string.
VALID_CONFIGS="dev"

## NOTE: For all supported instances, web server port number and pylons
## ".ini" file names must be specified here.
PORT_dev="5000"
PORT_prod=""
CONFIG_dev="development-othmar-pglocal.ini"
CONFIG_dev_test=""
CONFIG_prod=""
CONFIG_prod_test=""

## PostgreSQL Database configuration

## NOTE: EVERYTHING BELOW THIS LINE NOT CURRENTLY USED
## ----------------------------------------------------------------------------

## Database host
DB_HOST="localhost"

## Database port.
DB_PORT="5432"

## Database name.
DB_NAME="portal_beta"

## Database user name. This user must have privileges necessary to drop and
## recreate tables / functions within the given database.
DB_USER="portaladmin"

## Password for above user.
## NOTE: Auth type expected to be TCP/IP on localhost where password is required.
DB_PASS=""

## Database scripts to import (as the user setup above). Any scripts specified
## here will be imported in the order provided.
##
## NOTE: SQL scripts are only imported when the portal-admin command is "dbreload"
##
DB_SCRIPTS="$REPO_SRC/Portal/guide/model/abacus/pg_schema.sql \
            $REPO_SRC/Portal/guide/model/abacus/clearance-84.sql \
            $REPO_SRC/Portal/guide/model/abacus/ascc_users.sql"

## Flags for Magma schema build, passed to script $REPO_SRC/Portal/guide/model/magma/db/build.sh
## NOTE: Magma build is only performed when portal-admin command is "dbreload"
MAGMA_FLAGS="-c QFAB-magma.conf -d magma -N"
